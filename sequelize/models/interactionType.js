'use strict';

module.exports = function(sequelize, DataTypes) {
    var InteractionType = sequelize.define('interactionType', {
        id: {
            type: DataTypes.INTEGER,
            incrementMe: { type: DataTypes.INTEGER, autoIncrement: true },
            primaryKey: true
        },
        description: {
            type: DataTypes.STRING,
            allowNull: false,
            validate: {
                notEmpty: true
            }
        }
        
    },{
        timestamps: true,
        paranoid: true,
        freezeTableName: true,
        classMethods: {
            associate: function(models) {
                
            }
        }
    });
    return InteractionType;
};