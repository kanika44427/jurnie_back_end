# Instructions
Go through the following steps to set up this project.
* Open up a terminal and direct it to the root of this project
* Make sure you've installed `mysql`
* Restore the database by running this command:
```
mysql -u root -p inventory_management < backup.sql
```
* Create a non-root user named `client` with the password `wtaftw`
* Give `client` the right to use only `inventory_management` and only run the commands insert, update, delete and select
* npm install
* bower install
* enter the command node server in this terminal
* open another terminal to the root of this project and enter `gulp`

Any changes you make to the frontend code should be in the `public/src/` directory. This is because gulp is set-up to compile and compress anything in `/src` and apply it to `/dist` and update the browser with dist. If you change `/dist` directly, your changes won't take effect and will be overwritten.

If you have any questions, feel free to contact me.
