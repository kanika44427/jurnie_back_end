'use strict';

module.exports = function(sequelize, DataTypes) {
	const Interaction = sequelize.define(
		'interaction',
		{
			id: {
				type: DataTypes.UUID,
				defaultValue: DataTypes.UUIDV1,
				primaryKey: true
			}
		},
		{
			timestamps: true,
			paranoid: true,
			freezeTableName: true,
			classMethods: {
				associate: function(models) {
					Interaction.belongsTo(models.interactionType, { foreignKey: { required: true } });
					Interaction.belongsTo(models.user, { foreignKey: { required: true } });
					Interaction.belongsTo(models.pin);
					Interaction.belongsTo(models.user, {
						as: 'inviter'
					});
					Interaction.belongsTo(models.user, {
						as: 'receiver'
					});
					Interaction.belongsTo(models.invite);
				}
			}
		}
	);
	return Interaction;
};
