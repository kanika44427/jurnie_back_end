'use strict';

module.exports = {
	up: function(queryInterface, Sequelize) {
		var models = require('../models');
		return models.interaction.bulkCreate([
			{
				interactionTypeId: 1,
				userId: '2be93310-aad9-11e6-9053-059778d17327',
				inviteId: 'kjkj3310-aad9-11e6-9258-059778d17327',
				receiverId: '2ae93310-aad9-11e6-9258-059778d17327'
			},
			{
				interactionTypeId: 2,
				userId: '2ab93310-aad9-11e6-9028-059778d17327',
				inviterId: '2be93310-aad9-11e6-9053-059778d17327'
			},
			{
				interactionTypeId: 2,
				userId: '2ae93310-aad9-11e6-9258-059778d17327',
				inviterId: '2ab93310-aad9-11e6-9028-059778d17327'
			},
			{
				interactionTypeId: 3,
				userId: '2ab93310-aad9-11e6-9028-059778d17327',
				pinId: '2ab93310-aad9-11e6-9028-059778d17327'
			},
			{
				interactionTypeId: 4,
				userId: '2ab93310-aad9-11e6-9028-059778d17327',
				pinId: '2ab93310-aad9-11e6-9028-059778d17327'
			}
		]);
	},

	down: function(queryInterface, Sequelize) {
		var models = require('../models');
		return queryInterface.bulkDelete('interaction', null, {});
	}
};
