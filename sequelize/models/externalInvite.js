'use strict';

module.exports = function(sequelize, DataTypes) {
	const ExternalInvite = sequelize.define(
		'externalInvite',
		{
			id: {
				type: DataTypes.UUID,
				defaultValue: DataTypes.UUIDV1,
				primaryKey: true
			},
			fulfilled: {
				type: DataTypes.BOOLEAN,
				required: true,
				defaultValue: false
			}
		},
		{
			timestamps: true,
			createdAt: 'startDate',
			paranoid: true,
			freezeTableName: true,
			classMethods: {
				associate: function(models) {
					ExternalInvite.belongsTo(models.user, {
						foreignKey: { required: true, as: 'inviterId' },
						as: 'inviter'
					});
				}
			}
		}
	);
	return ExternalInvite;
};
