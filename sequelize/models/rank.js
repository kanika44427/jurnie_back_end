'use strict';

module.exports = function(sequelize, DataTypes) {
	var Rank = sequelize.define(
		'rank',
		{
			id: {
				type: DataTypes.UUID,
				defaultValue: DataTypes.UUIDV1,
				primaryKey: true
			},
			description: {
				type: DataTypes.STRING,
				allowNull: true,
				validate: {
					notEmpty: true
				}
			},
			pins: {
				type: DataTypes.INTEGER,
				allowNull: true
			}
		},
		{
			timestamps: true,
			paranoid: true,
			freezeTableName: true,
			classMethods: {
				associate: function(models) {}
			}
		}
	);
	return Rank;
};
