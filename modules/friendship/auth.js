var	auth = require('../../config/auth.js');

module.exports = {
    belongsToUserOrIsAdmin: belongsToUserOrIsAdmin,
    belongsToUser: belongsToUser
};

    
function belongsToUserOrIsAdmin(req,res,next){
    req.tokenType = 'user';
    auth.isAuthenticated(req,res,function(){
        auth.owns(req,res,'friendship',req.tokenPayload.id,function(response){
            if(response.success){
                next();
            }
            else{
                auth.hasRole(req,res,'admin',function(result){
                    if(result.success){
                        next();
                    }
                    else{
                        res.status(403);
                        res.json({message:'User not authorized for this information'});
                    }
                });
            }
        });
    });
}

function belongsToUser(req,res,next){
    req.tokenType = 'user';
    auth.isAuthenticated(req,res,function(){
        auth.owns(req,res,'invite',req.params.inviteId,function(response){
            if(response.success){
                next();
            }
            else{
                res.status(403);
                res.json({message:'User not authorized for this information'});
            }
        });
    });
}
    
