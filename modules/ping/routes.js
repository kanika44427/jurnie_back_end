var router = require('express').Router({ mergeParams: true });

router.route('/').get((req, res) => res.json({ message: 'success' }));

module.exports = router;
