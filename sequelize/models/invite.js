'use strict';

module.exports = function(sequelize, DataTypes) {
    var Invite = sequelize.define('invite', {
        id: {
            type: DataTypes.UUID,
            defaultValue: DataTypes.UUIDV1,
            primaryKey: true
        }
    },{
        timestamps: true,
        createdAt: 'startDate',
        paranoid: true,
        freezeTableName: true,
        classMethods: {
            associate: function(models) {
                Invite.belongsTo(models.user,{foreignKey:{required:true, as:'inviterId'}, as: 'inviter'});
                Invite.belongsTo(models.user,{foreignKey:{required:true, as:'inviteeId'}, as: 'invitee'});
            }
        }
    });
    return Invite;
};