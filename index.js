// Invoke 'strict' JavaScript mode
'use strict';

// Load the module dependencies
process.env.NODE_ENV = 'production';
if (process.platform === 'darwin') {
	process.env.NODE_ENV = 'development';
}
// process.env.NODE_ENV = process.env.NODE_ENV || 'development';

const app = require('express')(),
	http = require('http'),
	server = http.Server(app),
	auth = require('./config/auth.js'),
	samSetup = require('sam-setup'),
	samSequelize = require('sam-sequelize'),
	models = require('./sequelize/models'),
	path = require('path'),
	logger = require('./services/log'),
	getPort = require('get-port');

// if (process.env.MIGRATION) {
// 	samSequelize.migrate(models.sequelize, path.resolve('./sequelize/scripts'));
// } else if (process.env.INITIALIZE) {
// 	samSequelize.initialize(models.sequelize, path.resolve('./sequelize/scripts'));
// } else {
// 	samSequelize.run(models.sequelize);
// }

samSequelize.run(models.sequelize);
const port = process.env.MIGRATION ? 3001 : process.env.INITIALIZE ? 3002 : process.env.PORT || 3003;

samSetup(app, { reqsPerPeriod: 5500, env: 'tiny' });

require('./routes.js')(app);

try {
	server.listen(port, function() {
		console.log('Listening on port: ' + port);
		console.log('ENVIRONMENT: ' + process.env.NODE_ENV);
		logger.log('START_UP', {});
	});
} catch (e) {
	getPort().then(newPort => {
		server.listen(newPort, function() {
			console.log('Listening on port: ' + newPort);
			console.log('ENVIRONMENT: ' + process.env.NODE_ENV);
		});
	});
}
