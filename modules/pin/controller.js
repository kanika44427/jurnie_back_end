// Invoke "strict" JavaScript mode
'use strict';

// Load the module dependencies
const models = require('../../sequelize/models'),
	auth = require('./auth.js'),
	SamSequelize = require('sam-sequelize'),
	key = require('../../config/options.js').google.geocoding,
	tracking = require('../../services/tracking'),
	logger = require('../../services/log'),
	request = require('request');

module.exports = {
	list(req, res) {
		const options = {};
		if (req.query.userId) {
			//only pins for given user
			models.friendship
				.findAll({
					where: {
						frienderId: req.tokenPayload.id
					}
				})
				.then(friends => {
					const friendIds = friends.map(e => e.friendeeId);
					models.user
						.findById(req.query.userId, {
							include: [
								{
									model: models.pin,
									include: [
										models.pinType,
										{
											model: models.pin,
											on: {
												'$pins.city$': { $col: 'pins.nearbyPins.city' },
												'$pins.country$': { $col: 'pins.nearbyPins.country' },
												'$pins.nearbyPins.userId$': { $in: friendIds },
												'$pins.userId$': req.query.userId
											},
											as: 'nearbyPins',
											include: [
												{
													model: models.user,
													attributes: Object.keys(models.user.attributes).filter(
														attr => attr !== 'password'
													)
												}
											]
										}
									]
								}
							],
							attributes: Object.keys(models.user.attributes).filter(attr => attr !== 'password')
						})
						.then(function(user) {
							user.dataValues.pins.forEach(pin => {
								pin.dataValues.description =
									pin.dataValues.description ||
									pin.dataValues.city + ', ' + pin.dataValues.countryCode;
								pin.dataValues.nearbyPins.forEach(
									nearbyPin =>
										(nearbyPin.dataValues.description =
											nearbyPin.dataValues.description ||
											nearbyPin.dataValues.city + ', ' + nearbyPin.dataValues.countryCode)
								);
							});
							res.json(user.dataValues.pins);
						})
						.catch(err => {
							logger.log('ListPinsErr', err);
							SamSequelize.handleError(req, res, err);
						});
				})
				.catch(err => {
					logger.log('ListPinsErr', err);
					SamSequelize.handleError(req, res, err);
				});
		} else if (req.query.showFriends) {
			//pins for authenticated user and friends or only authenticated user's friends (depends on ?friends-only query param)
			models.friendship
				.findAll({
					where: { frienderId: req.tokenPayload.id }
				})
				.then(friends => {
					const friendIds = friends
						.map(el => el.dataValues.friendeeId)
						.concat(req.query.friendsOnly ? [] : [req.tokenPayload.id]);
					models.pin
						.findAll({
							include: [models.pinType, { model: models.user, exclude: ['password'] }],
							where: { userId: { $in: friendIds } }
						})
						.then(function(pins) {
							user.dataValues.pins.forEach(
								pin =>
									(pin.dataValues.description =
										pin.dataValues.description ||
										pin.dataValues.city + ', ' + pin.dataValues.countryCode)
							);
							res.json(pins);
						})
						.catch(err => {
							logger.log('ListPinsErr', err);
							SamSequelize.handleError(req, res, err);
						});
				})
				.catch(err => {
					logger.log('ListPinsErr', err);
					SamSequelize.handleError(req, res, err);
				});
		} else {
			models.friendship
				.findAll({
					where: {
						frienderId: req.tokenPayload.id
					}
				})
				.then(friends => {
					const friendIds = friends.map(e => e.friendeeId);
					logger.log('Getting pins for: ', req.tokenPayload);
					logger.log('Friends: ', friends);
					models.user
						.findById(req.tokenPayload.id, {
							include: [
								{
									model: models.pin,
									include: [
										models.pinType,
										{
											model: models.pin,
											on: {
												'$pins.city$': { $col: 'pins.nearbyPins.city' },
												'$pins.country$': { $col: 'pins.nearbyPins.country' },
												'$pins.nearbyPins.userId$': { $in: friendIds },
												'$pins.userId$': req.tokenPayload.id
											},
											as: 'nearbyPins',
											include: [
												{
													model: models.user,
													attributes: Object.keys(models.user.attributes).filter(
														attr => attr !== 'password'
													)
												}
											]
										}
									]
								}
							],
							attributes: Object.keys(models.user.attributes).filter(attr => attr !== 'password')
						})
						.then(function(user) {
							logger.log('User:', user);
							user.dataValues.pins = user.dataValues.pins || [];
							user.dataValues.pins.forEach(pin => {
								pin.dataValues.description =
									pin.dataValues.description ||
									pin.dataValues.city + ', ' + pin.dataValues.countryCode;
								pin.dataValues.nearbyPins.forEach(
									nearbyPin =>
										(nearbyPin.dataValues.description =
											nearbyPin.dataValues.description ||
											nearbyPin.dataValues.city + ', ' + nearbyPin.dataValues.countryCode)
								);
							});
							res.json(user.dataValues.pins);
						})
						.catch(err => {
							logger.log('ListPinsErr', err);
							SamSequelize.handleError(req, res, err);
						});
				})
				.catch(err => {
					logger.log('ListPinsErr', err);
					SamSequelize.handleError(req, res, err);
				});
		}
	},

	add(req, res) {
		req.body.userId = req.tokenPayload.id;
		pinAlreadyExists(req.body.userId, req.body.latitude, req.body.longitude)
			.then(exists => {
				if (exists) {
					return res.status(400).json({ message: 'You already have a pin at this location!' });
				}
				getPlaces(req, res, result => {
					if (!result.success) {
						res.status(400).json({ message: 'Failed to get City and Country from given coordinates' });
					}
					models.pin
						.create(req.body)
						.then(function(pin) {
							models.interaction
								.create({
									interactionTypeId: 3,
									pinId: pin.dataValues.id,
									userId: req.tokenPayload.id
								})
								.then(blah => {
									pin.dataValues.description =
										pin.dataValues.description ||
										pin.dataValues.city + ', ' + pin.dataValues.countryCode;
									tracking.event(
										req.tokenPayload.id,
										'CREATE_PIN',
										`CREATE_PIN_OF_TYPE_${mapPinTypeIdToPinType(pin.dataValues.pinTypeId)}`,
										pin.dataValues.description
									);
									res.json(pin);
								})
								.catch(err => {
									logger.log('AddPinsErr', err);
									SamSequelize.handleError(req, res, err);
								});
						})
						.catch(err => {
							logger.log('AddPinsErr', err);
							SamSequelize.handleError(req, res, err);
						});
				});
			})
			.catch(err => {
				SamSequelize.handleError(req, res, err);
			});
	},

	edit(req, res) {
		req.body.userId = req.tokenPayload.id;
		models.pin
			.update(req.body, {
				where: {
					id: req.params.pinId
				}
			})
			.then(function(updatedRecords) {
				tracking.event(req.tokenPayload.id, 'EDIT_PIN', 'EDIT_PIN', req.body.description);
				res.json(updatedRecords);
			})
			.catch(err => {
				logger.log('EditPinErr', err);
				SamSequelize.handleError(req, res, err);
			});
	},

	grab(req, res) {
		models.pin
			.findById(req.params.pinId)
			.then(function(pin) {
				if (!pin) return res.status(400).json({ message: 'No such pin exists' });
				pin.dataValues.description =
					pin.dataValues.description || pin.dataValues.city + ', ' + pin.dataValues.countryCode;
				res.json(pin);
			})
			.catch(err => {
				logger.log('GrabPinErr', err);
				SamSequelize.handleError(req, res, err);
			});
	},

	remove(req, res) {
		models.pin
			.findById(req.params.pinId)
			.then(pin => {
				models.pin
					.destroy({
						where: {
							id: req.params.pinId
						}
					})
					.then(function() {
						tracking.event(req.tokenPayload.id, 'DELETE_PIN', 'DELETE_PIN', pin.dataValues.description);
						res.json(pin);
					})
					.catch(err => {
						logger.log('RemovePinErr', err);
						SamSequelize.handleError(req, res, err);
					});
			})
			.catch(err => {
				logger.log('RemovePinErr', err);
				SamSequelize.handleError(req, res, err);
			});
	},

	getNearbyPins(req, res) {
		models.friendship
			.findAll({
				where: {
					frienderId: req.tokenPayload.id
				}
			})
			.then(friends => {
				const friendIds = friends.map(e => e.friendeeId);
				req.body.latitude = req.query.latitude;
				req.body.longitude = req.query.longitude;
				getPlaces(req, res, result => {
					if (!result.success) {
						return res.status(500).json({ message: 'Failed to get nearby places' });
					}
					models.pin
						.findAll({
							include: [models.pinType, { model: models.user, exclude: ['password'] }],
							where: {
								userId: { $in: friendIds },
								city: req.body.city,
								country: req.body.country
							}
						})
						.then(function(pins) {
							pins.forEach(
								pin =>
									(pin.dataValues.description =
										pin.dataValues.description ||
										pin.dataValues.city + ', ' + pin.dataValues.countryCode)
							);
							res.json(filterOutFriendMultiples(pins));
						})
						.catch(err => {
							logger.log('GetNearbyPinsErr', err);
							SamSequelize.handleError(req, res, err);
						});
				});
			});
	},

	pinId(req, res, next, id) {
		req.pinId = id;
		next();
	}
};

function getPlaces(req, res, next) {
	if (!req.body.latitude || !req.body.longitude) {
		return res.status(400).json({ message: 'latitude and longitude are required fields' });
	}
	request(
		`https://maps.googleapis.com/maps/api/geocode/json?latlng=${req.body.latitude},${req.body.longitude}&key=${
			key
		}`,
		function(error, response, body) {
			if (!error && response.statusCode == 200) {
				const results = parseOutPlaceDescription(JSON.parse(body));
				if (results.success) {
					req.body.city = results.city;
					req.body.country = results.country;
					req.body.countryCode = results.countryCode;
				}
				next({ success: results.success });
			} else {
				next({ success: false });
			}
		}
	);
}

function parseOutPlaceDescription(body) {
	const places = {
		country: null,
		locality: null,
		administrative_area_level_3: null,
		administrative_area_level_2: null,
		administrative_area_level_1: null
	};
	body.results.forEach(desc => {
		if (desc.types.includes('country')) {
			places.country = desc.address_components[0].long_name;
			places.countryCode = desc.address_components[0].short_name;
		} else if (desc.types.includes('locality')) {
			places.locality = desc.address_components[0].long_name;
		} else if (desc.types.includes('administrative_area_level_3')) {
			places.administrative_area_level_3 = desc.address_components[0].long_name;
		} else if (desc.types.includes('administrative_area_level_2')) {
			places.administrative_area_level_2 = desc.address_components[0].long_name;
		} else if (desc.types.includes('administrative_area_level_1')) {
			places.administrative_area_level_1 = desc.address_components[0].long_name;
		}
	});
	if (!places.country) {
		gleanCountry(places, body.results);
		if (!places.country) return { success: false };
	}
	if (!places.locality) {
		if (!places.administrative_area_level_2) {
			if (!places.administrative_area_level_1) {
				return { success: false };
			} else {
				return {
					success: true,
					city: places.administrative_area_level_1,
					country: places.country,
					countryCode: places.countryCode
				};
			}
		} else {
			return {
				success: true,
				city: places.administrative_area_level_2,
				country: places.country,
				countryCode: places.countryCode
			};
		}
	} else {
		return {
			success: true,
			city: places.locality,
			country: places.country,
			countryCode: places.countryCode
		};
	}
}

function gleanCountry(placesMap, placesList) {
	let countryDetails = null;
	if (placesMap.administrative_area_level_3) {
		countryDetails = getCountryDetails(placesMap.administrative_area_level_3, placesList);
	} else if (placesMap.administrative_area_level_2) {
		countryDetails = getCountryDetails(placesMap.administrative_area_level_2, placesList);
	} else if (placesMap.administrative_area_level_1) {
		countryDetails = getCountryDetails(placesMap.administrative_area_level_1, placesList);
	} else if (placesMap.locality) {
		countryDetails = getCountryDetails(placesMap.locality, placesList);
	}
	if (countryDetails) {
		placesMap.country = countryDetails.country;
		placesMap.countryCode = countryDetails.countryCode;
	}
}

function getCountryDetails(subCountryPlace, placesList) {
	const subCountryPlaceObj = getPlace(subCountryPlace, placesList);
	const countryName = getCountryName(subCountryPlaceObj);
	const countryCode = getCountryCode(subCountryPlaceObj, countryName);
	return {
		country: countryName,
		countryCode: countryCode
	};
}

function getCountryCode(subCountry, countryName) {
	return subCountry.address_components.reduce(
		(prev, curr) => (prev ? prev : curr.long_name === countryName ? curr.short_name : false),
		false
	);
}

function getCountryName(place) {
	const splitAddress = place.formatted_address.split(',');
	return splitAddress[splitAddress.length - 1].trim();
}

function getPlace(desc, list) {
	return list.reduce(
		(prev, curr) => (prev ? prev : curr.address_components[0].long_name === desc ? curr : false),
		false
	);
}

function mapPinTypeIdToPinType(id) {
	const types = {
		1: 'Been_There',
		2: 'Going_There',
		3: 'Want_To_Go'
	};
	return types[id];
}

function pinAlreadyExists(userId, lat, long) {
	return new Promise((resolve, reject) => {
		models.pin
			.findAll({
				where: {
					userId: userId,
					latitude: lat,
					longitude: long
				}
			})
			.then(function(pins) {
				resolve(!!pins.length);
			})
			.catch(function(error) {
				logger.log('pinAlreadyExistsErr', error);
				reject(error);
			});
	});
}

function filterOutFriendMultiples(pins) {
	const friends = {};
	return pins.filter(p => {
		if (!friends[p.userId]) {
			friends[p.userId] = true;
			return true;
		}
		return false;
	});
}
