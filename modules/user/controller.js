// Invoke "strict" JavaScript mode
'use strict';

// Load the module dependencies
const auth = require('./auth.js'),
	models = require('../../sequelize/models'),
	tracking = require('../../services/tracking'),
	logger = require('../../services/log'),
	email = require('../../services/email'),
	SamSequelize = require('sam-sequelize');

module.exports = {
	list(req, res) {
		models.user
			.findAll({
				attributes: { exclude: ['password'] }
			})
			.then(Users => res.json(Users))
			.catch(err => {
				logger.log('ListUserErr', err);
				SamSequelize.handleError(req, res, err);
			});
	},

	getNonFriend(req, res) {
		models.user
			.findById(req.params.userId, { attributes: ['firstName', 'lastName', 'id', 'profilePic'] })
			.then(user => res.json(user))
			.catch(err => {
				logger.log('GetNonFriendUserErr', err);
				SamSequelize.handleError(req, res, err);
			});
	},

	grab(req, res) {
		console.log("req", req, "res", res);
	// 	models.friendship
	// 		.findAll({
	// 			where: {
	// 				frienderId: req.tokenPayload.id
	// 			}
	// 		})
	// 		.then(friends => {
	// 			const friendIds = friends.map(e => e.friendeeId);
	// 			models.user
	// 				.findById(req.params.userId, {
	// 					include: [
	// 						models.userType,
	// 						{
	// 							model: models.pin,
	// 							include: [
	// 								models.pinType,
	// 								{
	// 									model: models.pin,
	// 									on: {
	// 										'$pins.city$': { $col: 'pins.nearbyPins.city' },
	// 										'$pins.country$': { $col: 'pins.nearbyPins.country' },
	// 										'$pins.nearbyPins.userId$': { $in: friendIds },
	// 										'$pins.userId$': req.params.userId
	// 									},
	// 									as: 'nearbyPins',
	// 									include: [
	// 										{
	// 											model: models.user,
	// 											attributes: Object.keys(models.user.attributes).filter(
	// 												attr => attr !== 'password'
	// 											)
	// 										}
	// 									]
	// 								}
	// 							]
	// 						},
	// 						models.travellerType
	// 					],
	// 					attributes: Object.keys(models.user.attributes).filter(attr => attr !== 'password')
	// 				})
	// 				.then(function(user) {
	// 					//console.log("user harsh", JSON.stringify(user));
	// 					models.rank
	// 						.findAll({
	// 							order: ['id']
	// 						})
	// 						.then(ranks => {
	// 							user.dataValues.rank = ranks.reduce(
	// 								(prev, curr) => {
	// 									return user.dataValues.pins.length >= prev.pins &&
	// 										user.dataValues.pins.length >= curr.dataValues.pins
	// 										? curr.dataValues
	// 										: prev;
	// 								},
	// 								{ pins: 0 }
	// 							);
	// 							user.dataValues.pins.forEach(pin => {
	// 								pin.dataValues.description =
	// 									pin.dataValues.description ||
	// 									pin.dataValues.city + ', ' + pin.dataValues.countryCode;
	// 								pin.dataValues.nearbyPins.forEach(
	// 									nearbyPin =>
	// 										(nearbyPin.dataValues.description =
	// 											nearbyPin.dataValues.description ||
	// 											nearbyPin.dataValues.city + ', ' + nearbyPin.dataValues.countryCode)
	// 								);
	// 							});
	// 							res.json(user);
	// 						})
	// 						.catch(err => {
	// 							logger.log('ListUserErr', err);
	// 							SamSequelize.handleError(req, res, err);
	// 						});
	// 				})
	// 				.catch(error => SamSequelize.handleError(req, res, error));
	// 		})
	// 		.catch(e => SamSequelize.handleError(req, res, err));
	 },

	add(req, res) {
		auth.signup(req, res, function(token) {
			if (req.body.emailInviteId || req.body.fbInviteId) {
				const inviteId = req.body.emailInviteId || req.body.fbInviteId;
				models.externalInvite
					.findById(inviteId)
					.then(invite => {
						models.friendship
							.create({
								frienderId: invite.dataValues.frienderId,
								friendeeId: req.userId
							})
							.then(invite => {
								models.interaction
									.create({
										interactionTypeId: 2,
										inviterId: req.userId,
										userId: invite.dataValues.inviterId
									})
									.then(() => {
										models.interaction
											.create({
												interactionTypeId: 2,
												inviterId: invite.dataValues.inviterId,
												userId: req.userId
											})
											.then(() => {
												tracking.event(
													req.userId,
													'NEW_USER',
													'NEW_USER',
													req.body.firstName + ' ' + req.body.lastName
												);
												tracking.event(
													req.userId,
													'SIGNUP_RESPONSE',
													'FAV_PLACE',
													req.body.favPlace
												);
												tracking.event(
													req.userId,
													'SIGNUP_RESPONSE',
													'NATIONALITY',
													req.body.nationality
												);
												tracking.event(
													req.userId,
													'SIGNUP_RESPONSE',
													'TRAVELLER_TYPE',
													getTravellerType(req.body.travellerTypeId)
												);
												tracking.event(req.userId, 'SIGNUP_RESPONSE', 'EMAIL', req.body.email);
												res.json({ token: token });
											})
											.catch(err => {
												logger.log('AddUserErr', err);
												SamSequelize.handleError(req, res, err);
											});
									})
									.catch(err => {
										logger.log('AddUserErr', err);
										SamSequelize.handleError(req, res, err);
									});
							})
							.catch(err => {
								logger.log('AddUserErr', err);
								SamSequelize.handleError(req, res, err);
							});
					})
					.catch(err => {
						logger.log('AddUserErr', err);
						SamSequelize.handleError(req, res, err);
					});
			} else {
				tracking.event(req.userId, 'NEW_USER', 'NEW_USER', req.body.firstName + ' ' + req.body.lastName);
				tracking.event(req.userId, 'SIGNUP_RESPONSE', 'FAV_PLACE', req.body.favPlace);
				tracking.event(req.userId, 'SIGNUP_RESPONSE', 'NATIONALITY', req.body.nationality);
				tracking.event(req.userId, 'SIGNUP_RESPONSE', 'EMAIL', req.body.email);
				tracking.event(
					req.userId,
					'SIGNUP_RESPONSE',
					'TRAVELLER_TYPE',
					getTravellerType(req.body.travellerTypeId)
				);
				res.json({ token: token });
			}
		});
	},

	addSocialUser(req, res, next, id) {
		let userType = req.params.platform;
		//console.log("userType ... ",userType);
		res.json("Hello...");
	},

	edit(req, res) {
		models.user
			.update(req.body, {
				where: {
					id: req.params.userId
				},
				attributes: { exclude: ['password'] }
			})
			.then(updatedRecords => module.exports.grab(req, res))
			.catch(err => {
				logger.log('EditUserErr', err);
				SamSequelize.handleError(req, res, err);
			});
	},

	remove(req, res) {
		models.user
			.destroy({
				where: {
					id: req.params.userId
				},
				attributes: { exclude: ['password'] }
			})
			.then(deletedRecords => {
				models.interaction
					.destroy({
						where: {
							$or: {
								userId: req.params.userId,
								receiverId: req.params.userId,
								inviterId: req.params.userId
							}
						}
					})
					.then(deletedRecords => {
						models.friendship
							.destroy({
								where: {
									$or: {
										friendeeId: req.params.userId,
										frienderId: req.params.userId
									}
								}
							})
							.then(deletedRecords => {
								models.pin
									.destroy({
										where: {
											userId: req.params.userId
										}
									})
									.then(deletedRecords => {
										res.json(deletedRecords);
									})
									.catch(err => {
										logger.log('RemoveUserErr', err);
										SamSequelize.handleError(req, res, err);
									});
							})
							.catch(err => {
								logger.log('RemoveUserErr', err);
								SamSequelize.handleError(req, res, err);
							});
					})
					.catch(err => {
						logger.log('RemoveUserErr', err);
						SamSequelize.handleError(req, res, err);
					});
			})
			.catch(err => {
				logger.log('RemoveUserErr', err);
				SamSequelize.handleError(req, res, err);
			});
	},

	forgotPassword(req, res) {
		models.user
			.findOne({
				where: {
					email: req.body.email
				}
			})
			.then(user => {
				if (!user) return res.status(400).json({ message: 'No such email exists' });

				models.forgotPassword
					.create({
						email: req.body.email
					})
					.then(fp => {
						//email
						email
							.send({
								template: 'forgot-password',
								values: { url: `https://www.jurnie.com/#!/reset-password?code=${fp.dataValues.id}` },
								to: req.body.email,
								subject: 'Reset Password'
							})
							.then(info => {
								res.json({ message: 'Success' });
							})
							.catch(err => {
								res.status(500).json({ message: 'Unable to successfully send email' });
							});
					})
					.catch(err => {
						logger.log('ForgotPasswordErr', err);
						SamSequelize.handleError(req, res, err);
					});
			})
			.catch(err => {
				logger.log('ForgotPasswordErr', err);
				SamSequelize.handleError(req, res, err);
			});
	},

	resetPassword(req, res) {
		models.forgotPassword
			.findById(req.body.code, {
				where: {
					createdAt: {
						$gt: new Date() - 5 * 60 * 1000
					},
					email: req.body.email
				}
			})
			.then(code => {
				if (!code) return res.status(400).json({ message: 'Either code is invalid or has already expired.' });

				models.forgotPassword
					.destroy({
						where: {
							id: req.body.code
						}
					})
					.then(fp => {
						auth.updatePassword(req, res, err => {
							if (err) return res.status(400).json({ message: 'Failed to update password' });
							res.json({ msg: 'success' });
						});
					})
					.catch(err => {
						logger.log('ResetPasswordErr', err);
						SamSequelize.handleError(req, res, err);
					});
			})
			.catch(err => {
				logger.log('ResetPasswordErr', err);
				SamSequelize.handleError(req, res, err);
			});
	},

	getMe(req, res) {
		req.params.userId = req.tokenPayload.id;
		//console.log("userid" + req.params.userId);
		module.exports.grab(req, res);
	},

	userId(req, res, next, id) {
		req.userId = id;
		next();
	},

	managerEmail(req, res, next, id) {
		req.mgrEmail = id;
		next();
	}

	

};

function getTravellerTypeFromId(id) {
	const types = {
		1: 'The Backpacker',
		2: 'The Relaxer',
		3: 'The Explorer',
		4: 'The High Roller',
		5: 'The Thrill-Seeker',
		6: 'The Pilgrim-er',
		7: 'The Retiree',
		8: 'The Tagalong',
		9: 'The Researcher'
	};
	return types[id];
}

function getTravellerType(id) {
	var types = {
		1: 'The Backpacker',
		2: 'The Relaxer',
		3: 'The Explorer',
		4: 'The High Roller',
		5: 'The Thrill-Seeker',
		6: 'The Pilgrim-er',
		7: 'The Retiree',
		8: 'The Tagalong',
		9: 'The Researcher'
	};
	return types[id];
}
