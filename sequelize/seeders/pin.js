'use strict';

module.exports = {
	up: function(queryInterface, Sequelize) {
		var models = require('../models');
		return models.pin.bulkCreate([
			{
				latitude: 40,
				longitude: 40,
				startDate: Date.now(),
				endDate: Date.now(),
				rating: 3,
				note: 'SOOO COOOL!',
				pinTypeId: 1,
				countryCode: 'TR',
				country: 'Turkey',
				city: 'Otlukbuli',
				userId: '2ae93310-aad9-11e6-9258-059778d17327'
			},
			{
				id: '2be93310-aad9-11e6-9053-059778d17327',
				latitude: 40,
				longitude: 40,
				startDate: Date.now(),
				endDate: Date.now(),
				rating: 3,
				note: 'SOOO COOOL!',
				pinTypeId: 2,
				countryCode: 'TR',
				country: 'Turkey',
				city: 'Otlukbuli',
				userId: '2be93310-aad9-11e6-9053-059778d17327'
			},
			{
				id: '2ab93310-aad9-11e6-9028-059778d17327',
				latitude: 40,
				longitude: 40,
				startDate: Date.now(),
				endDate: Date.now(),
				rating: 3,
				note: 'SOOO COOOL!',
				pinTypeId: 3,
				countryCode: 'TR',
				country: 'Turkey',
				city: 'Otlukbuli',
				userId: '2ab93310-aad9-11e6-9028-059778d17327'
			},
			{
				latitude: 40.257514,
				longitude: -111.66303,
				startDate: Date.now(),
				endDate: Date.now(),
				rating: 3,
				note: 'WTA',
				pinTypeId: 1,
				countryCode: 'USA',
				country: 'United States of America',
				city: 'Provo',
				userId: '2ae93310-aad9-11e6-9258-059778d17327'
			},
			{
				latitude: 40.250118,
				longitude: -111.662464,
				startDate: Date.now(),
				endDate: Date.now(),
				rating: 4,
				note: 'McDonalds',
				pinTypeId: 1,
				countryCode: 'USA',
				country: 'United States of America',
				city: 'Provo',
				userId: '2ae93310-aad9-11e6-9258-059778d17327'
			},
			{
				latitude: 40.226232,
				longitude: -111.660899,
				startDate: Date.now(),
				endDate: Date.now(),
				rating: 5,
				note: 'Startup Building',
				pinTypeId: 1,
				countryCode: 'USA',
				country: 'United States of America',
				city: 'Provo',
				userId: '2ae93310-aad9-11e6-9258-059778d17327'
			},
			{
				id: '7beqwe10-xxxx-yyyy-zzzz-059778d17327',
				latitude: 40.257514,
				longitude: -111.66303,
				startDate: Date.now(),
				endDate: Date.now(),
				rating: 3,
				note: 'WTA',
				pinTypeId: 1,
				countryCode: 'USA',
				country: 'United States of America',
				city: 'Provo',
				userId: '2be93310-aad9-11e6-9053-059778d17327'
			},
			{
				latitude: 40.250118,
				longitude: -111.662464,
				startDate: Date.now(),
				endDate: Date.now(),
				rating: 4,
				note: 'McDonalds',
				pinTypeId: 1,
				countryCode: 'USA',
				country: 'United States of America',
				city: 'Provo',
				userId: '2be93310-aad9-11e6-9053-059778d17327'
			},
			{
				latitude: 40.226232,
				longitude: -111.660899,
				startDate: Date.now(),
				endDate: Date.now(),
				rating: 5,
				note: 'Startup Building',
				pinTypeId: 1,
				countryCode: 'USA',
				country: 'United States of America',
				city: 'Provo',
				userId: '2be93310-aad9-11e6-9053-059778d17327'
			}
		]);
	},

	down: function(queryInterface, Sequelize) {
		var models = require('../models');
		return queryInterface.bulkDelete('pin', null, {});
	}
};
