var router = require('express').Router({ mergeParams: true }),
	auth = require('./auth.js'),
	userAuth = require('../user/auth.js'),
	controller = require('./controller.js');

router
	.route('/')
	.post(userAuth.isLoggedIn, controller.add)
	.get(userAuth.isLoggedIn, controller.list);

router.route('/nearby').get(userAuth.isLoggedIn, controller.getNearbyPins);

router
	.route('/:pinId')
	.get(userAuth.isLoggedIn, controller.grab)
	.put(auth.belongsToUserOrIsAdmin, controller.edit)
	.delete(auth.belongsToUserOrIsAdmin, controller.remove);

router.param('pinId', controller.pinId);

module.exports = router;
