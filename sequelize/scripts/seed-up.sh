#!/bin/bash

#up
echo "Seed:Up"

sequelize db:seed --seed travellerType.js
sequelize db:seed --seed usertype.js
sequelize db:seed --seed user.js
sequelize db:seed --seed friendship.js
sequelize db:seed --seed externalInvite.js
sequelize db:seed --seed invite.js

sequelize db:seed --seed pinType.js
sequelize db:seed --seed pin.js

sequelize db:seed --seed interactionType.js
sequelize db:seed --seed interaction.js

sequelize db:seed --seed city.js
sequelize db:seed --seed rank.js


