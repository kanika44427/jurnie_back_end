const nodemailer = require('nodemailer'),
	ejs = require('ejs'),
	AWSConfig = require('../config/options').aws,
	MANDRILL_KEY = require('../config/options').mandrill.key,
	lo = require('lodash'),
	fs = require('fs'),
	aws = require('aws-sdk'),
	path = require('path'),
	FROM = 'info@thejurnie.com',
	mandrillTransport = require('nodemailer-mandrill-transport'),
	// transporter = nodemailer.createTransport({
	// 	SES: new aws.SES({
	// 		apiVersion: '2010-12-01',
	// 		region: 'eu-west-1',
	// 		accessKeyId: AWSConfig.accessKeyId,
	// 		secretAccessKey: AWSConfig.secretAccessKey
	// 	})
	// }),
	transporter = nodemailer.createTransport(
		mandrillTransport({
			auth: {
				apiKey: MANDRILL_KEY
			}
		})
	);

module.exports = {
	send: send
};

function send(options, cb) {
	return new Promise((resolve, reject) => {
		try {
			fs.readFile(path.join(__dirname, `./emailTemplates/${options.template}.ejs`), 'utf-8', (err, data) => {
				if (err) return reject(err);
				options.html = ejs.compile(data)(options.values);
				options.from = FROM;
				const finalOptions = lo.pick(options, ['from', 'to', 'subject', 'attachments', 'html']);
				transporter.sendMail(finalOptions, (err, info) => {
					if (err || info.rejected.length) return reject(err || info.rejected);
					console.log(info.envelope);
					console.log(info.messageId);
					resolve(info);
				});
			});
		} catch (e) {
			reject(e);
		}
	});
}
