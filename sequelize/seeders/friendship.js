'use strict';

module.exports = {
	up: function(queryInterface, Sequelize) {
		var models = require('../models');
		return models.friendship.bulkCreate([
			{
				id: 'der93310-qqqq-11e6-9053-059778d17327',
				frienderId: '2ae93310-aad9-11e6-9258-059778d17327',
				friendeeId: '2ab93310-aad9-11e6-9028-059778d17327'
			},
			{
				id: 'lkl93310-qqqq-11e6-9053-059778d17327',
				frienderId: '2ab93310-aad9-11e6-9028-059778d17327',
				friendeeId: '2ae93310-aad9-11e6-9258-059778d17327'
			},
			{
				id: 'www93310-qqqq-11e6-9053-059778d17327',
				frienderId: '2ab93310-aad9-11e6-9028-059778d17327',
				friendeeId: '2be93310-aad9-11e6-9053-059778d17327'
			},
			{
				id: 'qqq93310-qqqq-11e6-9053-059778d17327',
				frienderId: '2be93310-aad9-11e6-9053-059778d17327',
				friendeeId: '2ab93310-aad9-11e6-9028-059778d17327'
			}
		]);
	},

	down: function(queryInterface, Sequelize) {
		var models = require('../models');
		return queryInterface.bulkDelete('friendship', null, {});
	}
};
