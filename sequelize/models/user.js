'use strict';

module.exports = function(sequelize, DataTypes) {
	var User = sequelize.define(
		'user',
		{
			id: {
				type: DataTypes.UUID,
				defaultValue: DataTypes.UUIDV1,
				primaryKey: true
			},
			firstName: {
				type: DataTypes.STRING,
				validate: {
					notEmpty: true
				},
				allowNull: false
			},			
			lastName: {
				type: DataTypes.STRING,
				validate: {
					notEmpty: true
				},
				allowNull: false
			},			
			email: {
				type: DataTypes.STRING,
				unique: true,
				validate: {
					notEmpty: true,
					isEmail: true
				},
				allowNull: false
			},
			password: {
				type: DataTypes.STRING.BINARY,
				validate: {
					notEmpty: true
				},
				allowNull: false
			},
			gender: {
				type: DataTypes.STRING,
				validate: {
					isGender(value) {
						if (!['male', 'female'].includes(value.toLowerCase())) {
							throw new Error('Only "male" or "female" allowed for gender');
						}
					}
				},
				allowNull: false
			},
			phone: {
				type: DataTypes.STRING,
				allowNull: true,
				validate: {
					notEmpty: true
				}
			},
			profilePic: {
				type: DataTypes.STRING,
				allowNull: false,
				validate: {
					notEmpty: true
				},
				defaultValue: 'assets/Annonymous.png'
			},
			favPlace: {
				type: DataTypes.STRING,
				allowNull: true
			},
			birthday: {
				type: DataTypes.DATE
			},
			nationality: {
				type: DataTypes.STRING
			}
			
		},
		{
			timestamps: true,
			createdAt: 'startDate',
			paranoid: false,
			freezeTableName: true,
			classMethods: {
				associate: function(models) {
					User.belongsTo(models.userType, {
						foreignKey: { allowNull: false }
					});
					User.hasMany(models.pin);
					User.hasMany(models.interaction);
					User.belongsToMany(models.user, {
						through: { model: models.friendship, unique: false },
						foreignKey: {
							as: 'frienderId',
							required: true
						},
						as: 'friends',
						otherKey: 'friendeeId'
					});
					User.belongsToMany(models.user, {
						through: { model: models.friendship, unique: false },
						foreignKey: {
							as: 'friendeeId',
							required: true
						},
						as: 'frienders',
						otherKey: 'frienderId'
					});
					User.belongsTo(models.travellerType);
				},
				getterMethods: {
					fullName: function() {
						return this.firstName + ' ' + this.lastName;
					}
				}
			}
		}
	);
	return User;
};
