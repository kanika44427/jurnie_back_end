var awsImgUploader = require('aws-img-uploader');

module.exports = {
	// add: uploader.add
	add: awsImgUploader({
		bucket: 'jurnie-profile-pictures',
		cfLoc: 'https://E1EP12WYK12WS6.cloudfront.net',
		auth: require('../../config/options').aws,
		region: 'ap-southeast-2'
	}).add
};
