var router = require('express').Router({ mergeParams: true }),
	userAuth = require('../user/auth.js'),
	controller = require('./controller.js');

router.route('/').get(userAuth.isAdmin, controller.list);

module.exports = router;
