'use strict';

module.exports = {
	up: function(queryInterface, Sequelize) {
		
		const models = require('../models');
		return models.user.bulkCreate([
			{
				firstName: 'Sam',
				lastName: 'User',
				gender: 'male',
				email: 'user@test.com',
				password: '$2a$08$Y7ay3Rl/kPKzJ7hGP2wTCeEYtivwp1pN2riDrbQ9X9MNE80RjL4/.',
				id: '2ae93310-aad9-11e6-9258-059778d17327',
				travellerTypeId: 1,
				profilePic: 'http://lorempixel.com/100/100/people/',
				userTypeId: 3,
				nationality: "'Murican",
				birthday: Date.now(),
				preferenceId: '2be93310-abd9-11e6-9053-059778d18327'
			},
			{
				firstName: 'Sam',
				lastName: 'User2',
				gender: 'male',
				email: 'user2@test.com',
				password: '$2a$08$Y7ay3Rl/kPKzJ7hGP2wTCeEYtivwp1pN2riDrbQ9X9MNE80RjL4/.',
				id: '2ab93310-aad9-11e6-9028-059778d17327',
				travellerTypeId: 2,
				nationality: "'Murican",
				birthday: Date.now(),
				profilePic: 'http://lorempixel.com/100/100/people/',
				userTypeId: 3
			},
			{
				firstName: 'Sam',
				lastName: 'User3',
				gender: 'male',
				email: 'user3@test.com',
				password: '$2a$08$Y7ay3Rl/kPKzJ7hGP2wTCeEYtivwp1pN2riDrbQ9X9MNE80RjL4/.',
				id: '2be93310-aad9-11e6-9053-059778d17327',
				travellerTypeId: 3,
				nationality: "'Murican",
				birthday: Date.now(),
				profilePic: 'http://lorempixel.com/100/100/people/',
				userTypeId: 3
			},
			{
				firstName: 'Sam',
				lastName: 'Admin',
				gender: 'male',
				email: 'admin@test.com',
				password: '$2a$08$Y7ay3Rl/kPKzJ7hGP2wTCeEYtivwp1pN2riDrbQ9X9MNE80RjL4/.',
				id: '92406390-aad8-11e6-92a1-c91d4751ea24',
				travellerTypeId: 1,
				nationality: "'Murican",
				birthday: Date.now(),
				profilePic: 'http://lorempixel.com/100/100/people/',
				userTypeId: 1
			}
		]);
	},

	down: function(queryInterface, Sequelize) {
		const models = require('../models');
		return queryInterface.bulkDelete('user', null, {});
	}
};
