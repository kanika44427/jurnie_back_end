'use strict';

module.exports = function(sequelize, DataTypes) {
    var City = sequelize.define('city', {
        id: {
            type: DataTypes.UUID,
            defaultValue: DataTypes.UUIDV1,
            primaryKey: true
        },
        city: {
            type: DataTypes.STRING,
            allowNull: false,
            validate: {
                notEmpty: true
            }
        },
        country: {
            type: DataTypes.STRING,
            allowNull: false,
            validate: {
                notEmpty: true
            }
        },
        latitude: {
            type: DataTypes.DOUBLE,
            allowNull: false
        },
        longitude: {
            type: DataTypes.DOUBLE,
            allowNull: false 
        } 
    },{
        timestamps: true,
        paranoid: true,
        freezeTableName: true,
        classMethods: {
            associate: function(models) {
                // City.belongsToMany(models.user,{through:'user_city'});
                // City.belongsToMany(models.group,{through:'group_city'});
            }
        }
    });
    return City;
};