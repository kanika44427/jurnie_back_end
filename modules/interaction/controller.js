// Invoke "strict" JavaScript mode
'use strict';

// Load the module dependencies
const models = require('../../sequelize/models'),
	SamSequelize = require('sam-sequelize'),
	logger = require('../../services/log'),
	Sequelize = require('sequelize');

module.exports = {
	list(req, res) {
		models.userInteraction
			.findAll({
				where: {
					userId: req.tokenPayload.id
				}
			})
			.then(seen => {
				const seenIds = seen.map(ui => ui.dataValues.interactionId);
				getInteractions(req, res, seenIds, false).then(interactions => {
					interactions = interactions || [];
					interactions.forEach(i => {
						i.dataValues.isNew = true;
					});
					if (interactions.length && interactions.length >= 6) {
						res.json(interactions);
					} else {
						getInteractions(req, res, seenIds, true).then(seenInteractions => {
							if (seenInteractions && seenInteractions.length) {
								while (interactions.length < 6 && seenInteractions.length) {
									const oldI = seenInteractions.shift();
									oldI.dataValues.isNew = false;
									interactions.push(oldI);
								}
								res.json(interactions);
							} else {
								res.json(interactions);
							}
						});
					}
				});
			})
			.catch(err => {
				logger.log('ListSeenInteractionsErr', err);
				SamSequelize.handleError(req, res, err);
			});
	},

	grab(req, res) {},

	add(body, cb) {
		models.interaction
			.create(body)
			.then(result => cb(null, result))
			.catch(err => cb(err));
	},

	markAsRead(req, res) {
		models.userInteraction
			.create({ interactionId: req.interactionId, userId: req.tokenPayload.id })
			.then(results => {
				res.json({ message: 'success' });
			})
			.catch(err => {
				logger.log('markAsSeenErr', err);
				SamSequelize.handleError(req, res, err);
			});
	},

	interactionId(req, res, next, id) {
		req.interactionId = id;
		next();
	}
};

function addMessages(req, interactions) {
	interactions.map(interaction => {
		if (interaction.dataValues.interactionTypeId === 1) {
			interaction.dataValues.message = `${interaction.dataValues.user.firstName} ${interaction.dataValues.user
				.lastName} added you.`;
		} else if (interaction.dataValues.interactionTypeId === 2) {
			const isFriender = interaction.dataValues.inviterId === req.tokenPayload.id;
			const isFriendee = interaction.dataValues.userId === req.tokenPayload.id;
			if (isFriender) {
				interaction.dataValues.message = `You are now friends with ${interaction.dataValues.user
					.firstName} ${interaction.dataValues.user.lastName}.`;
			} else if (isFriendee) {
				interaction.dataValues.message = `You are now friends with ${interaction.dataValues.inviter
					.firstName} ${interaction.dataValues.inviter.lastName}.`;
			} else {
				interaction.dataValues.message = `${interaction.dataValues.user.firstName} ${interaction.dataValues.user
					.lastName} is now friends with ${interaction.dataValues.inviter.firstName} ${interaction.dataValues
					.inviter.lastName}.`;
			}
		} else if (interaction.dataValues.interactionTypeId === 3) {
			interaction.dataValues.message = `${interaction.dataValues.user.firstName} ${interaction.dataValues.user
				.lastName} added a new pin${interaction.dataValues.pin
				? ` for ${interaction.dataValues.pin.city}, ${interaction.dataValues.pin.country}`
				: ''}`;
		} else if (interaction.dataValues.interactionTypeId === 4) {
			interaction.dataValues.message = `${interaction.dataValues.user.firstName} ${interaction.dataValues.user
				.lastName} added a new note for ${interaction.dataValues.pin.city}, ${interaction.dataValues.pin
				.country}`;
		}
	});
	return interactions;
}

function consolidateNewPins(results) {
	const pinners = {};
	let max = 6;
	for (let i = 0; i < max && i < results.length; i++) {
		let result = results[i];
		if (result.dataValues.interactionTypeId === 3) {
			if (!pinners[result.dataValues.userId]) {
				pinners[result.dataValues.userId] = [];
			} else {
				max++;
			}
			pinners[result.dataValues.userId].push(result);
		}
	}
	for (let i = max; i < results.length; i++) {
		let result = results[i];
		if (result.dataValues.interactionTypeId === 3 && pinners[result.dataValues.userId]) {
			pinners[result.dataValues.userId].push(result);
		}
	}
	for (let id in pinners) {
		if (pinners.hasOwnProperty(id)) {
			let newResult = pinners[id][0];
			newResult.dataValues.message =
				pinners[id].length === 1
					? newResult.dataValues.message
					: `${newResult.dataValues.user.firstName} ${newResult.dataValues.user.lastName} added ${pinners[id]
							.length} new pins!`;
		}
	}
	return results
		.map(result => {
			if (result.dataValues.interactionTypeId === 3) {
				if (pinners[result.dataValues.userId]) {
					const newResult = pinners[result.dataValues.userId][0];
					delete pinners[result.dataValues.userId];
					return newResult;
				} else {
					return null;
				}
			} else {
				return result;
			}
		})
		.filter(result => result)
		.slice(0, 6);
}

function getInteractions(req, res, seenIds, getSeen) {
	seenIds = seenIds && seenIds.length ? seenIds : ['blah-totally-just-here-to-prevent-sequelize-null-bug'];

	const seenSettings = getSeen
		? {
				$in: seenIds
			}
		: {
				$notIn: seenIds
			};

	return new Promise((resolve, reject) => {
		models.friendship
			.findAll({
				where: { frienderId: req.tokenPayload.id }
			})
			.then(friends => {
				const friendIds = [...friends.map(el => el.dataValues.friendeeId), 1];
				models.interaction
					.findAll({
						where: {
							id: seenSettings,
							$or: [
								{
									interactionTypeId: { $in: [3, 4] },
									userId: {
										$in: friendIds
									}
								},
								// {
								// 	interactionTypeId: 2,
								// 	userId: {
								// 		$in: friendIds
								// 	},
								// 	inviterId: {
								// 		$not: req.tokenPayload.id
								// 	}
								// },
								{
									interactionTypeId: 2,
									inviterId: {
										$in: friendIds
									},
									userId: req.tokenPayload.id
								},
								{
									receiverId: req.tokenPayload.id,
									interactionTypeId: 1,
									inviterId: { $notIn: friendIds }
								}
							],
							createdAt: { $gt: new Date().getDate() - 7 }
						},
						order: [['createdAt', 'DESC'], ['interactionTypeId']],
						limit: 30,
						include: [
							{
								model: models.user,
								as: 'inviter',
								attributes: Object.keys(models.user.attributes).filter(attr => attr !== 'password')
							},
							models.pin,
							models.invite,
							{
								model: models.user,
								attributes: Object.keys(models.user.attributes).filter(attr => attr !== 'password')
							}
						]
					})
					.then(function(interactions) {
						const resultsWithMsgs = addMessages(req, interactions);
						const results = consolidateNewPins(resultsWithMsgs);
						resolve(results);
					})
					.catch(err => {
						logger.log('ListInteractionsErr', err);
						SamSequelize.handleError(req, res, err);
					});
			})
			.catch(err => {
				logger.log('ListInteractionsErr', err);
				SamSequelize.handleError(req, res, err);
			});
	});
}
