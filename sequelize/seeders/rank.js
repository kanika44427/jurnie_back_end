'use strict';

module.exports = {
	up: function(queryInterface, Sequelize) {
		var models = require('../models');
		return models.rank.bulkCreate([
			{
				id: 1,
				description: 'Dreamer',
				pins: 0
			},
			{
				id: 2,
				description: 'Baby Steps',
				pins: 1
			},
			{
				id: 3,
				description: 'Amateur Traveller',
				pins: 5
			},
			{
				id: 4,
				description: 'Seasoned Traveller',
				pins: 10
			},
			{
				id: 5,
				description: 'Avid Traveller',
				pins: 20
			},
			{
				id: 6,
				description: 'Expert Traveller',
				pins: 50
			},
			{
				id: 7,
				description: 'Globe Trotter',
				pins: 100
			}
		]);
	},

	down: function(queryInterface, Sequelize) {
		var models = require('../models');
		return queryInterface.bulkDelete('rank', null, {});
	}
};
