'use strict';

module.exports = function(sequelize, DataTypes) {
	var Pin = sequelize.define(
		'pin',
		{
			id: {
				type: DataTypes.UUID,
				defaultValue: DataTypes.UUIDV1,
				primaryKey: true
			},
			latitude: {
				type: DataTypes.DOUBLE,
				allowNull: false
			},
			longitude: {
				type: DataTypes.DOUBLE,
				allowNull: false
			},
			city: {
				type: DataTypes.STRING,
				allowNull: false
			},
			country: {
				type: DataTypes.STRING,
				allowNull: false
			},
			countryCode: {
				type: DataTypes.STRING,
				allowNull: false
			},
			startDate: {
				type: DataTypes.DATE,
				allowNull: true
			},
			endDate: {
				type: DataTypes.DATE,
				allowNull: true
			},
			rating: {
				type: DataTypes.INTEGER,
				allowNull: false
			},
			note: {
				type: DataTypes.TEXT,
				allowNull: true
			},
			description: {
				type: DataTypes.STRING
			}
		},
		{
			timestamps: true,
			paranoid: true,
			freezeTableName: true,
			classMethods: {
				associate: function(models) {
					Pin.belongsTo(models.user, { foreignKey: { require: true } });
					Pin.belongsTo(models.pinType, { foreignKey: { require: true } });
					Pin.hasMany(models.pin, { as: 'nearbyPins' });
				}
			}
		}
	);
	return Pin;
};
