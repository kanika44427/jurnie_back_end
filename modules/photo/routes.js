var router = require('express').Router({ mergeParams: true }),
	userAuth = require('../user/auth.js'),
	controller = require('./controller.js');

router.route('/').post(controller.add);

module.exports = router;
