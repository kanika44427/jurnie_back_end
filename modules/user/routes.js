const router = require('express').Router({ mergeParams: true }),
	auth = require('./auth.js'),
	controller = require('./controller.js');

router.route('/logout').post(auth.logout);

router.route('/signup').post(auth.isValidRecaptcha, controller.add);

router.route('/socialsignup/').post(auth.socialSignup);

router.route('/sociallogin/').post(auth.socialLogin);


router.route('/login').post(auth.login);

router.route('/me').get(auth.isLoggedIn, controller.getMe);

router.route('/forgot-pw').post(controller.forgotPassword);
router.route('/reset-pw').post(controller.resetPassword);

router.route('/non-friend/:userId').get(auth.isLoggedIn, controller.getNonFriend);

router
	.route('/:userId')
	.get(auth.isUserOrFriendOrAdmin, controller.grab) //is this user, friend, or admin
	.put(auth.isThisUserOrAdmin, controller.edit)
	.delete(auth.isThisUserOrAdmin, controller.remove);

router.param('userId', controller.userId);

module.exports = router;
