'use strict';

module.exports = {
	up: function(queryInterface, Sequelize) {
		var models = require('../models');
		return models.travellerType.bulkCreate([
			{
				id: 1,
				description: 'The Backpacker'
			},
			{
				id: 2,
				description: 'The Relaxer'
			},
			{
				id: 3,
				description: 'The Explorer'
			},
			{
				id: 4,
				description: 'The High Roller'
			},
			{
				id: 5,
				description: 'The Thrill-Seeker'
			},
			{
				id: 6,
				description: 'The Pilgrim-er'
			},
			{
				id: 7,
				description: 'The Retiree'
			},
			{
				id: 8,
				description: 'The Tagalong'
			},
			{
				id: 9,
				description: 'The Researcher'
			}
		]);
	},

	down: function(queryInterface, Sequelize) {
		var models = require('../models');
		return queryInterface.bulkDelete('travellerType', null, {});
	}
};
