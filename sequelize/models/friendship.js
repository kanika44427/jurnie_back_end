'use strict';

module.exports = function(sequelize, DataTypes) {
	var Friendship = sequelize.define(
		'friendship',
		{
			id: {
				type: DataTypes.UUID,
				defaultValue: DataTypes.UUIDV1,
				primaryKey: true
			}
		},
		{
			timestamps: true,
			createdAt: 'startDate',
			paranoid: true,
			freezeTableName: true,
			classMethods: {
				associate: function(models) {
					Friendship.belongsTo(models.user, {
						foreignKey: { required: true, as: 'frienderId' },
						as: 'friender',
						unique: false
					});
					Friendship.belongsTo(models.user, {
						foreignKey: { required: true, as: 'friendeeId' },
						as: 'friendee',
						unique: false
					});
				}
			}
		}
	);
	return Friendship;
};
