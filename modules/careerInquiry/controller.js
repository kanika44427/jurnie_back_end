// Invoke "strict" JavaScript mode
'use strict';

// Load the module dependencies
const models = require('../../sequelize/models'),
	SamSequelize = require('sam-sequelize'),
	multer = require('multer'),
	fs = require('fs-extra'),
	storage = multer.diskStorage({
		destination: function(req, file, cb) {
			cb(null, './temp');
		},
		filename: function(req, file, cb) {
			cb(null, Date.now() + file.originalname);
		}
	}),
	multerOptions = {
		storage: storage,
		limits: { fileSize: 1000 * 1000 }
	},
	logger = require('../../services/log'),
	Email = require('../../services/email'),
	upload = multer(multerOptions).single('file');

module.exports = {
	list(req, res) {
		models.careerInquiry
			.findAll()
			.then(function(careerInquirys) {
				res.json(careerInquirys);
			})
			.catch(function(error) {
				logger.log('ListCareerInquiriesErr', error);
				SamSequelize.handleError(req, res, error);
			});
	},

	add(req, res) {
		models.careerInquiry
			.create(req.body)
			.then(function(careerInquiry) {
				res.json(careerInquiry);
			})
			.catch(function(error) {
				logger.log('AddCareerInquiryErr', error);
				SamSequelize.handleError(req, res, error);
			});
	},

	resume(req, res) {
		upload(req, res, err => {
			if (err) {
				logger.log('FileUploadErr', err);
			}
			if (err && err.code === 'LIMIT_FILE_SIZE') {
				return res.status(400).json({ message: 'Uploaded file is too large' });
			} else if (err) return res.status(400).json({ message: 'Bad File' });
			const mailOptions = {
				to: 'warwick.ralston@gmail.com',
				subject: 'New Resume Submission',
				attachments: [
					{
						path: __dirname + '/../../temp/' + req.file.filename
					}
				],
				template: 'resume',
				values: {}
			};
			Email.send(mailOptions).then(function(error, info) {
				if (error) {
					logger.log('SendMailErr', err);
					return res.status(500).json({ message: 'failed to send resume to Jurnie team - please try again' });
				}
				fs.remove(__dirname + '/../../temp/' + req.file.filename, err => {
					if (err) {
						logger.log('Failed to delete resume');
					}
					console.log('Message sent: ' + info.response);
					res.json({ message: 'success' });
				});
			});
		});
	},

	grab(req, res) {
		models.careerInquiry
			.findById(req.params.careerInquiryId, {
				include: [models.userType]
			})
			.then(function(careerInquiry) {
				res.json(careerInquiry);
			})
			.catch(function(error) {
				logger.log('GrabCareerInquiryErr', error);
				SamSequelize.handleError(req, res, error);
			});
	},

	remove(req, res) {
		models.careerInquiry
			.destroy({
				where: {
					id: req.params.careerInquiryId
				}
			})
			.then(function(deletedRecords) {
				res.json(deletedRecords);
			})
			.catch(function(error) {
				logger.log('RemoveCareerInquiryErr', error);
				SamSequelize.handleError(req, res, error);
			});
	},

	careerInquiryId(req, res, next, id) {
		req.careerInquiryId = id;
		next();
	}
};
