// Invoke "strict" JavaScript mode
'use strict';

// Load the module dependencies
var models = require('../../sequelize/models'),
	SamSequelize = require('sam-sequelize'),
	tracking = require('../../services/tracking'),
	logger = require('../../services/log'),
	Sequelize = require('sequelize');

module.exports = {
	listRandom(req, res) {
		models.pin
			.findAll({
				where: {
					userId: req.tokenPayload.id
				}
			})
			.then(function(pins) {
				const lats = pins.map(p => p.dataValues.latitude);
				const longs = pins.map(p => p.dataValues.longitude);
				models.city
					.find({
						where: {
							$or: {
								latitude: { $notIn: lats },
								longitude: { $notIn: longs }
							}
						},
						order: [Sequelize.fn('RAND')],
						limit: 1
					})
					.then(function(city) {
						res.status(200).json(city);
					})
					.catch(function(error) {
						logger.log('ListRandomCityErr', error);
						SamSequelize.handleError(req, res, error);
					});
			})
			.catch(function(error) {
				logger.log('ListRandomCityErr', error);
				SamSequelize.handleError(req, res, error);
			});
	},

	add(req, res) {
		models.city
			.create(req.body)
			.then(function(city) {
				res.status(200).json(city);
			})
			.catch(function(error) {
				logger.log('AddCityErr', error);
				SamSequelize.handleError(req, res, error);
			});
	},

	edit(req, res) {
		models.city
			.update(req.body, {
				where: {
					id: req.params.cityId
				}
			})
			.then(function(updatedRecords) {
				res.status(200).json(updatedRecords);
			})
			.catch(function(error) {
				logger.log('EditCityErr', error);
				SamSequelize.handleError(req, res, error);
			});
	},

	grab(req, res) {
		models.city
			.findById(req.params.cityId, {
				include: [models.userType]
			})
			.then(function(city) {
				res.status(200).json(city);
			})
			.catch(function(error) {
				logger.log('GrabCityErr', error);
				SamSequelize.handleError(req, res, error);
			});
	},

	remove(req, res) {
		models.city
			.destroy({
				where: {
					id: req.params.cityId
				}
			})
			.then(function(deletedRecords) {
				res.status(200).json(deletedRecords);
			})
			.catch(function(error) {
				logger.log('RemoveCityErr', error);
				SamSequelize.handleError(req, res, error);
			});
	},

	cityId(req, res, next, id) {
		req.cityId = id;
		next();
	}
};
