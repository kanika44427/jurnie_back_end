#!/bin/bash

#down
echo "Seed:Down"

sequelize db:seed:undo --seed rank.js
sequelize db:seed:undo --seed city.js

sequelize db:seed:undo --seed interaction.js
sequelize db:seed:undo --seed interactionType.js

sequelize db:seed:undo --seed pin.js
sequelize db:seed:undo --seed pinType.js

sequelize db:seed:undo --seed invite.js
sequelize db:seed:undo --seed externalInvite.js
sequelize db:seed:undo --seed friendship.js
sequelize db:seed:undo --seed user.js
sequelize db:seed:undo --seed usertype.js
sequelize db:seed:undo --seed travellerType.js
