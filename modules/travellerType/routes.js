var router = require('express').Router({mergeParams: true}),  
    
    userAuth = require('../user/auth.js'),
    controller = require('./controller.js');

router.route('/')
	.post(userAuth.isAdmin, controller.add)
    .get(controller.list);

router.route('/:travellerTypeId')
    .get(userAuth.isLoggedIn, controller.grab)
    .put(userAuth.isAdmin, controller.edit)
    .delete(userAuth.isAdmin, controller.remove);

router.param('travellerTypeId', controller.travellerTypeId);

module.exports = router;

