// Invoke "strict" JavaScript mode
'use strict';

// Load the module dependencies
const models = require('../../sequelize/models'),
	SamSequelize = require('sam-sequelize'),
	logger = require('../../services/log'),
	request = require('request');

module.exports = {
	list(req, res) {
		const startDate = req.query.startDate || null;
		const endDate = req.query.endDate || null;
		getPinsInRange(startDate, endDate)
			.then(pinData => {
				getSignupsInRange(startDate, endDate)
					.then(userData => {
						getAggregateData(startDate, endDate)
							.then(aggrData => {
								res.json({
									signups: userData.users,
									activeSignupCount: userData.activeCount,
									pins: pinData.pins,
									pinnerCount: pinData.userCount,
									aggregate: aggrData
								});
							})
							.catch(err => {
								SamSequelize.handleError(req, res, err);
							});
					})
					.catch(err => {
						SamSequelize.handleError(req, res, err);
					});
			})
			.catch(err => {
				SamSequelize.handleError(req, res, err);
			});
	}
};

function getPinsInRange(startDate, endDate) {
	const condition = {
		where: {
			startDate: {
				$lte: +endDate || new Date()
			}
		},
		include: [
			models.pinType,
			{
				model: models.user,
				attributes: ['id']
			}
		],
		attributes: ['city', 'country']
	};
	if (startDate) {
		condition.where.startDate = { $gte: +startDate };
	}

	return new Promise((resolve, reject) => {
		const users = {};
		models.pin
			.findAll(condition)
			.then(results => {
				const pins = results.map(r => r.toJSON());
				console.log('pins', JSON.stringify(pins));
				resolve({
					pins: pins,
					userCount: pins.reduce((count, pin) => {
						console.log('pin', pin);
						if (users[pin.user.id]) {
							return count;
						} else {
							users[pin.user.id] = true;
							return ++count;
						}
					}, 0)
				});
			})
			.catch(err => {
				logger.log('getPinsInRangeErr', err);
				reject(err);
			});
	});
}

function getSignupsInRange(startDate, endDate) {
	const condition = {
		where: {
			startDate: {
				$lte: +endDate || Date.now()
			}
		},
		include: [/*{ model: models.pin, attributes: ['id'] }*/ models.pin, models.userType, models.travellerType],
		attributes: ['firstName', 'lastName', 'email', 'gender', 'favPlace', 'birthday', 'nationality', 'startDate']
	};
	if (startDate) {
		condition.where.startDate = { $gte: +startDate };
	}
	return new Promise((resolve, reject) => {
		models.user
			.findAll(condition)
			.then(results => {
				const users = results.map(r => {
					const newR = r.toJSON();
					newR.pins = newR.pins.length;
					return newR;
				});
				console.log('users', JSON.stringify(users));
				resolve({
					users: users,
					activeCount: users.reduce((count, user) => (user.pins ? ++count : count), 0)
				});
			})
			.catch(err => {
				logger.log('getSignupsInRangeErr', err);
				reject(err);
			});
	});
}

// new Date().setDate(today.getDate()-30) // 30 days ago
function getSignupStatsInRange(startDate, endDate) {
	const condition = {
		where: {
			startDate: +endDate || Date.now()
		},
		include: [models.pin, models.userType, models.travellerType],
		attributes: ['gender', 'favPlace', 'birthday', 'nationality', 'startDate']
	};
	if (startDate) {
		condition.where.startDate = { $gte: +startDate };
	}
	return new Promise((resolve, reject) => {
		models.user
			.findAll(condition)
			.then(results => {
				const users = results.map(r => r.toJSON());
				console.log('users', JSON.stringify(users));
				resolve({
					users: users,
					activeCount: users.reduce((count, user) => (user.pins && user.pins.length ? ++count : count), 0)
				});
			})
			.catch(err => {
				logger.log('getSignupsInRangeErr', err);
				reject(err);
			});
	});
}

function getAggregateData(startDate, endDate) {
	return new Promise((resolve, reject) => {
		getAges(startDate, endDate)
			.then(ages => {
				getNationalities(startDate, endDate)
					.then(natData => {
						getGenders(startDate, endDate).then(genderData => {
							resolve({ ages: ages, nationalities: natData, genders: genderData });
						});
					})
					.catch(err => reject(err));
			})
			.catch(err => reject(err));
	});
}

function getAges(startDate, endDate) {
	return new Promise((resolve, reject) => {
		const condition = {
			where: {
				startDate: {
					$lte: +endDate || Date.now()
				},
				birthday: {
					$ne: null
				}
			},
			attributes: [
				[
					models.sequelize.fn(
						'timestampdiff',
						models.sequelize.literal('year'),
						models.sequelize.col('birthday'),
						models.sequelize.fn('curdate')
					),
					'age'
				],
				[models.sequelize.fn('count', 'age'), 'count']
			],
			group: ['age']
		};
		if (startDate) {
			condition.where.startDate = { $gte: +startDate };
		}
		models.user
			.findAll(condition)
			.then(results => {
				const ageData = results.map(r => r.toJSON());
				console.log('ageData', JSON.stringify(ageData));
				resolve(ageData);
			})
			.catch(err => {
				logger.log('getAgesErr', err);
				reject(err);
			});
	});
}
function getNationalities(startDate, endDate) {
	return new Promise((resolve, reject) => {
		const condition = {
			where: {
				startDate: {
					$lte: +endDate || Date.now()
				},
				nationality: {
					$and: [{ $ne: null }, { $ne: 'Nationality' }]
				}
			},
			attributes: ['nationality', [models.sequelize.fn('count', 'nationality'), 'count']],
			group: ['nationality']
		};
		if (startDate) {
			condition.where.startDate = { $gte: +startDate };
		}
		models.user
			.findAll(condition)
			.then(results => {
				const nationalities = results.map(r => r.toJSON());
				console.log('nationalities', JSON.stringify(nationalities));
				resolve(nationalities);
			})
			.catch(err => {
				logger.log('getNationalitiesErr', err);
				reject(err);
			});
	});
}
function getGenders(startDate, endDate) {
	return new Promise((resolve, reject) => {
		const condition = {
			where: {
				startDate: {
					$lte: +endDate || Date.now()
				},
				gender: {
					$ne: null
				}
			},
			attributes: ['gender', [models.sequelize.fn('count', 'gender'), 'count']],
			group: ['gender']
		};
		if (startDate) {
			condition.where.startDate = { $gte: +startDate };
		}
		models.user
			.findAll(condition)
			.then(results => {
				const genders = results.map(r => r.toJSON());
				console.log('genders', JSON.stringify(genders));
				resolve(genders);
			})
			.catch(err => {
				logger.log('getGendersErr', err);
				reject(err);
			});
	});
}
