var router = require('express').Router({ mergeParams: true }),
	auth = require('./auth.js'),
	userAuth = require('../user/auth.js'),
	controller = require('./controller.js');

router
	.route('/')
	.post(userAuth.isThisUser, controller.add)
	.get(userAuth.isLoggedIn, controller.list);

router.route('/invite').post(userAuth.isLoggedIn, controller.invite);
router.route('/invite/bulk').post(userAuth.isLoggedIn, controller.inviteBulk);

router.route('/invite/email').post(userAuth.isLoggedIn, controller.emailInvite);
router.route('/invite/email/:emailInviteId').get(controller.getEmailInviter);

router.route('/invite/fb').post(userAuth.isLoggedIn, controller.fbInvite);
router.route('/invite/fb/:fbInviteId').get(controller.getFBInviter);

router
	.route('/respond/:inviteId')
	.post(auth.belongsToUser, controller.accept)
	.delete(auth.belongsToUser, controller.decline);

router.route('/:friendshipId').delete(auth.belongsToUserOrIsAdmin, controller.remove);

router.param('friendshipId', controller.friendshipId);

module.exports = router;
