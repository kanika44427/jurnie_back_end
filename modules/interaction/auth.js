var auth = require('../../config/auth.js');

module.exports = {
	belongsToUserOrIsAdmin: belongsToUserOrIsAdmin
};

function belongsToUserOrIsAdmin(req, res, next) {
	req.tokenType = 'user';
	auth.isAuthenticated(req, res, function() {
		auth.owns(req, res, 'interaction', req.interactionId, function(response) {
			if (response.success) {
				next();
			} else {
				auth.hasRole(req, res, 'admin', function(result) {
					if (result.success) {
						next();
					} else {
						res.status(403);
						res.json({ message: 'User not authorized for this information' });
					}
				});
			}
		});
	});
}
