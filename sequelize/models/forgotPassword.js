'use strict';

module.exports = function(sequelize, DataTypes) {
	var ForgotPassword = sequelize.define(
		'forgotPassword',
		{
			id: {
				type: DataTypes.UUID,
				defaultValue: DataTypes.UUIDV1,
				primaryKey: true
			},
			email: {
				type: DataTypes.STRING,
				allowNull: false,
				validate: {
					notEmpty: true
				}
			}
		},
		{
			timestamps: true,
			paranoid: true,
			freezeTableName: true,
			classMethods: {}
		}
	);
	return ForgotPassword;
};
