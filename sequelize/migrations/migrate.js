module.exports = {
    up: function(queryInterface, Sequelize) {
      // logic for transforming into the new state
      queryInterface.addColumn(
        'User',
        'userScocialType',
        Sequelize.STRING
      );
  
    },
  
    down: function(queryInterface, Sequelize) {
      // logic for reverting the changes
      
    }
  }