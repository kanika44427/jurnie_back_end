'use strict';

module.exports = function(sequelize, DataTypes) {
    var UserType = sequelize.define('userType', {
        id: {
            type: DataTypes.INTEGER,
            incrementMe: { type: DataTypes.INTEGER, autoIncrement: true },
            primaryKey: true
        },
        description: {
            type: DataTypes.STRING,
            allowNull: false
        }
    },{
        timestamps: true,
        paranoid: true,
        freezeTableName: true
    });
    return UserType;
};