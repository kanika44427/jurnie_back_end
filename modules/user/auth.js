var Models = require('../../sequelize/models/index.js'),
	SamSequelize = require('sam-sequelize'),
	Google = require('../../config/options').google,
	logger = require('../../services/log'),
	auth = require('../../config/auth.js'),
	request = require('request');

module.exports = {
	login: login,
	signup: signup,
	socialSignup : socialSignup,
	socialLogin : socialLogin,
	logout: auth.logout,
	isLoggedIn: isLoggedIn,
	isAdmin: isAdmin,
	isThisUser: isThisUser,
	isThisUserOrAdmin: isThisUserOrAdmin,
	isManagerOrAdmin: isManagerOrAdmin,
	getUserType: getUserType,
	isUserOrFriendOrAdmin: isUserOrFriendOrAdmin,
	isValidRecaptcha: isValidRecaptcha,
	updatePassword: updatePassword
};

function login(req, res) {
	req.tokenType = 'user';
	Models.user
		.findOne({ where: { email: req.body.email }, include: [Models.userType] })
		.then(function(user) {
			user = user && user.hasOwnProperty('dataValues') ? user.dataValues : null;
			if (user && auth.passwordsMatch(req.body.password, user.password)) {
				var token = auth.generateToken(
					'user',
					user.firstName,
					user.lastName,
					user.email,
					user.userType.dataValues.description,
					user.id
				);
				res.json({ token: token });
			} else {
			//res.status(401).send({"status":0,"message":"Invalid user"});
				
				res.status(401).send({ message: 'Username or Password were incorrect' });
			}
		
			//res.json({ token: token });
		})
		.catch(function(err) {
			console.error(err);
			//res.status(500);
		//	res.send();
		});
}

function updatePassword(req, res, cb) {
	req.tokenType = 'user';
	if (!req.body.password) return cb('No password received');
	req.body.password = auth.encryptPassword(req.body.password);
	Models.user
		.update(
			{ password: req.body.password },
			{
				where: {
					email: req.body.email
				}
			}
		)
		.then(user => {
			if (!user) {
				return res.status(500).json({ message: 'Internal Server Error' });
			}
			cb(null);
		})
		.catch(err => {
			logger.log('ResetPasswordErr', err);
			SamSequelize.handleError(req, res, err);
		});
}
function socialLogin(req, res, cb){
	let payload = {
		"email" : req.body.email,		
		"userType" : req.body.user_type,
		"providerId" : req.body.provider_id
	}
	Models.socialUser.findOne({ where : { 
		email : req.body.email, userType : req.body.user_type, providerId : req.body.provider_id
	}}).then(user => {
		//console.log("user", user.userType);
		//user = user && user.hasOwnProperty('dataValues') ? user.dataValues : null;
		if(user){
			var token = auth.generateToken(
						'user',
						user.firstName,
						user.lastName,
						user.email,
						user.providerId,
						//'',
						//user.userType,
						user.id
					);
					res.json({ token: token });
		}else{
			res.status(401).send({"status":0,"message":"Invalid user"});
		}
	},err=>{
		res.send(500)
	})

}

function socialSignup(req, res, cb) {
	
	let payload = {
		"email" : req.body.email,
		"firstName" : req.body.first_name,
		"lastName" : req.body.last_name,
		"userType" : req.body.user_type,
		"providerId" : req.body.provider_id,
		"profileImage" : req.body.profile_image,
	}


	Models.socialUser.findOne({ where : { 
		email : req.body.email, userType : req.body.user_type, providerId : req.body.provider_id
	}}).then(user => {
		if(!user){
			Models.socialUser.create(payload).then(result =>{
				res.status(200).send({"status":1,"message":"Thank you for registration using facebook"});
			},err =>{
				res.status(500).send(err);
			});
		}else{
			res.status(200).send({"status":0,"message":"User already registered"});
		}
	},err =>{
		res.send(500);
	});

}

function signup(req, res, cb) {
	req.tokenType = 'user';
	if (!req.body.password) return res.status(400).json({ message: 'Password is required' });
	req.body.password = auth.encryptPassword(req.body.password);
	req.body.userTypeId = 3;
	Models.user
		.create(req.body, { include: [Models.userType] })
		.then(function(newUser) {
			Models.userType
				.findOne({ where: { id: req.body.userTypeId } })
				.then(function(userType) {
					newUser = newUser.dataValues;
					req.userId = newUser.id;
					var token = auth.generateToken(
						'user',
						newUser.firstName,
						newUser.lastName,
						newUser.email,
						userType.dataValues.description,
						newUser.id
					);
					cb(token);
				})
				.catch(err => {
					logger.log('SignupUserErr', err);
					SamSequelize.handleError(req, res, err);
				});
		})
		.catch(err => {
			logger.log('SignupUserErr', err);
			SamSequelize.handleError(req, res, err);
		});
}

function isLoggedIn(req, res, next) {
	req.tokenType = 'user';
	auth.isAuthenticated(req, res, function() {
		next();
	});
}

function isAdmin(req, res, next) {
	req.tokenType = 'user';
	auth.isAuthenticated(req, res, function() {
		auth.hasRole(req, res, 'admin', function(result) {
			if (result.success) {
				next();
			} else {
				res.status(403);
				res.json({ message: 'User not authorized for this information' });
			}
		});
	});
}

function isThisUserOrAdmin(req, res, next) {
	req.tokenType = 'user';
	auth.isAuthenticated(req, res, function() {
		auth.owns(req, res, 'user', req.userId, function(response) {
			if (response.success) {
				next();
			} else {
				auth.hasRole(req, res, 'admin', function(result) {
					if (result.success) {
						next();
					} else {
						res.status(403);
						res.json({ message: 'User not authorized for this information' });
					}
				});
			}
		});
	});
}

function isThisUser(req, res, next) {
	req.tokenType = 'user';
	auth.isAuthenticated(req, res, function() {
		auth.owns(req, res, 'user', req.userId, function(response) {
			if (response.success) {
				next();
			} else {
				res.status(403);
				res.json({ message: 'User not authorized for this information' });
			}
		});
	});
}

function isUserOrFriendOrAdmin(req, res, next) {
	req.tokenType = 'user';
	auth.isAuthenticated(req, res, function() {
		auth.owns(req, res, 'user', req.userId, function(response) {
			if (response.success) return next();
			auth.hasRole(req, res, 'admin', function(result) {
				if (result.success) {
					next();
				} else {
					Models.friendship
						.findAll({
							where: {
								frienderId: req.params.userId
							}
						})
						.then(friends => {
							const friendIds = friends.map(e => e.friendeeId);
							if (friendIds.includes(req.tokenPayload.id)) {
								next();
							} else {
								res.status(403).json({ message: 'User not authorized for this information' });
							}
						})
						.catch(err => {
							logger.log('isUserOrFriendOrAdminErr', err);
							SamSequelize.handleError(req, res, err);
						});
				}
			});
		});
	});
}

function isManagerOrAdmin(req, res, next) {
	req.tokenType = 'user';
	auth.isAuthenticated(req, res, function() {
		auth.hasRole(req, res, ['manager', 'admin'], function(result) {
			if (result.success) {
				next();
			} else {
				res.status(403).json({ message: 'User not authorized for this information' });
			}
		});
	});
}

function getUserType(req, res, next) {
	req.tokenType = 'user';
	auth.getTokenInfo(req, res, next);
}

function isValidRecaptcha(req, res, next) {
	request.post(
		{
			url: `https://www.google.com/recaptcha/api/siteverify`,
			formData: { secret: Google.recaptcha, response: req.body.recaptchaResponse }
		},
		function(error, response, body) {
			if (!error && JSON.parse(body).success) return next();
			res.status(400).json({ message: 'Failed to pass recaptcha' });
		}
	);
}
