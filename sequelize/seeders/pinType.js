'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
      var models = require ('../models');
        return models.pinType.bulkCreate([
            {
                id: 1,
                description: 'Been There'
            },
            {
                id: 2,
                description: 'Going There'
            },
            {
                id: 3,
                description: 'Want To Go'
            }
        ]);
  },

  down: function (queryInterface, Sequelize) {
    var models = require('../models');
    return queryInterface.bulkDelete('pinType', null, {});
  }
};
