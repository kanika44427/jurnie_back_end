// Invoke "strict" JavaScript mode
'use strict';

// Load the module dependencies
const models = require('../../sequelize/models'),
	SamSequelize = require('sam-sequelize'),
	Sequelize = require('sequelize'),
	logger = require('../../services/log'),
	googleKeys = require('../../config/options').google,
	request = require('request');

module.exports = {
	searchPlaces(req, res) {
		const url = `https://maps.googleapis.com/maps/api/place/autocomplete/json?key=${googleKeys.places}&input=${req
			.query.searchText}&location=0,0&radius=20000000&offset=3`;
		request(url, function(error, response, body) {
			const places = JSON.parse(body).predictions.map(place => {
				return {
					description: {
						main: place.structured_formatting.main_text,
						secondary: place.structured_formatting.secondary_text
					},
					placeId: place.place_id,
					types: place.types
				};
			});
			models.sequelize
				.query(
					'SELECT CONCAT(firstName," ",lastName) AS name, id, profilePic FROM user WHERE CONCAT(firstName," ",lastName) LIKE :search_text AND id != :user_id LIMIT 5',
					{
						replacements: { search_text: `%${req.query.searchText}%`, user_id: req.tokenPayload.id },
						type: Sequelize.QueryTypes.SELECT
					}
				)
				.then(people => {
					res.json({ people: people, places: places });
				})
				.catch(err => {
					logger.log('SearchErr', err);
					SamSequelize.handleError(req, res, err);
				});
		});
	},
	reversePlaceId(req, res) {
		const url = `https://maps.googleapis.com/maps/api/geocode/json?place_id=${req.query
			.place_id}&key=${googleKeys.geocoding}`;
		request(url, function(error, response, body) {
			if (error) {
				logger.log('SearchNearbyErr', error);
			}
			const jsonBody = JSON.parse(body);
			res.json(jsonBody.results[0].geometry.location);
		});
	},
	searchNearby(req, res) {
		const url = `https://maps.googleapis.com/maps/api/place/nearbysearch/json?key=${googleKeys.places}&location=${req
			.query.lat},${req.query.long}&radius=100`;
		request(url, function(error, response, body) {
			if (error) {
				logger.log('SearchNearbyErr', error);
			}
			const places = JSON.parse(body).results.map(place => {
				return {
					location: place.geometry.location,
					description: place.name,
					placeId: place.place_id,
					types: place.types,
					vicinity: place.vicinity
				};
			});
			res.json(places.slice(0, 10));
		});
	}
};
