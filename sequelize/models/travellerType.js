'use strict';

module.exports = function(sequelize, DataTypes) {
	var TravellerType = sequelize.define(
		'travellerType',
		{
			id: {
				type: DataTypes.UUID,
				defaultValue: DataTypes.UUIDV1,
				primaryKey: true
			},
			description: {
				type: DataTypes.STRING,
				allowNull: true,
				validate: {
					notEmpty: true
				}
			}
		},
		{
			timestamps: true,
			paranoid: true,
			freezeTableName: true,
			classMethods: {
				associate: function(models) {}
			}
		}
	);
	return TravellerType;
};
