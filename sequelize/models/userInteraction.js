'use strict';

module.exports = function(sequelize, DataTypes) {
	const UserInteraction = sequelize.define(
		'userInteraction',
		{
			id: {
				type: DataTypes.UUID,
				defaultValue: DataTypes.UUIDV1,
				primaryKey: true
			}
		},
		{
			timestamps: true,
			paranoid: true,
			freezeTableName: true,
			classMethods: {
				associate: function(models) {
					UserInteraction.belongsTo(models.interaction, { foreignKey: { required: true } });
					UserInteraction.belongsTo(models.user, { foreignKey: { required: true } });
				}
			}
		}
	);
	return UserInteraction;
};
