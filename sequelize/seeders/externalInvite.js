'use strict';

module.exports = {
	up: function(queryInterface, Sequelize) {
		var models = require('../models');
		return models.externalInvite.bulkCreate([
			{
				id: 'kjkj3310-aad9-11e6-9258-059778d17327',
				inviterId: '2be93310-aad9-11e6-9053-059778d17327'
			}
		]);
	},

	down: function(queryInterface, Sequelize) {
		var models = require('../models');
		return queryInterface.bulkDelete('externalInvite', null, {});
	}
};
