const router = require('express').Router(),
	normalizedPath = require('path').join(__dirname, 'modules'),
	fs = require('fs-extra');

module.exports = function(app) {
	require('fs').readdirSync(normalizedPath).forEach(function(directory) {
		if (directory !== '.DS_Store') {
			fs.access(normalizedPath + '/' + directory + '/routes.js', fs.constants.R_OK, err => {
				if (!err) app.use('/api/v1/' + directory, require(normalizedPath + '/' + directory + '/routes.js'));
			});
		}
	});
};
