// const lawgs = require('lawgs'),
// 	AWS = require('../config/options');

// lawgs.config({
// 	aws: {
// 		accessKeyId: AWS.accessKeyId,
// 		secretAccessKey: AWS.secretAccessKey,
// 		region: AWS.region
// 	}
// });

// const logger = lawgs.getOrCreate('Jurnie');

// module.exports.log = function(eventName, dataObj) {
// 	logger.log(eventName, dataObj);
// };

module.exports.log = function(eventName, dataObj) {
	dataObj = typeof dataObj === 'object' ? dataObj : { info: dataObj };
	console.log(`${eventName}:`, JSON.stringify(dataObj));
};
