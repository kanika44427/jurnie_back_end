// Invoke "strict" JavaScript mode
'use strict';

// Load the module dependencies
const models = require('../../sequelize/models'),
	auth = require('./auth.js'),
	Email = require('../../services/email'),
	logger = require('../../services/log'),
	SamSequelize = require('sam-sequelize');

module.exports = {
	list(req, res) {
		models.friendship
			.findAll({
				where: {
					frienderId: req.tokenPayload.id
				}
			})
			.then(function(friendships) {
				res.status(200).json(friendships);
			})
			.catch(err => {
				logger.log('ListFriendshipErr', err);
				SamSequelize.handleError(req, res, err);
			});
	},

	add(req, res) {
		models.friendship
			.create(req.body)
			.then(function(friendship) {
				res.status(200).json(friendship);
			})
			.catch(err => {
				logger.log('AddFriendshipErr', err);
				SamSequelize.handleError(req, res, err);
			});
	},

	edit(req, res) {
		models.friendship
			.update(req.body, {
				where: {
					id: req.params.friendshipId
				}
			})
			.then(function(updatedRecords) {
				res.status(200).json(updatedRecords);
			})
			.catch(err => {
				logger.log('EditFriendshipErr', err);
				SamSequelize.handleError(req, res, err);
			});
	},

	grab(req, res) {
		models.friendship
			.findById(req.params.friendshipId, {
				include: [models.userType]
			})
			.then(function(friendship) {
				res.status(200).json(friendship);
			})
			.catch(err => {
				logger.log('GrabFriendshipErr', err);
				SamSequelize.handleError(req, res, err);
			});
	},

	remove(req, res) {
		models.friendship
			.destroy({
				where: {
					id: req.params.friendshipId
				}
			})
			.then(function(deletedRecords) {
				res.status(200).json(deletedRecords);
			})
			.catch(err => {
				logger.log('DeleteFriendshipErr', err);
				SamSequelize.handleError(req, res, err);
			});
	},

	invite(req, res) {
		models.invite
			.create({
				inviterId: req.tokenPayload.id,
				inviteeId: req.body.inviteeId
			})
			.then(invite => {
				models.interaction
					.create({
						interactionTypeId: 1,
						inviteId: invite.dataValues.id,
						receiverId: req.body.inviteeId,
						inviterId: req.tokenPayload.id,
						userId: req.tokenPayload.id
					})
					.then(blah => res.json(invite))
					.catch(err => {
						logger.log('InviteFriendshipErr', err);
						SamSequelize.handleError(req, res, err);
					});
			})
			.catch(err => {
				logger.log('InviteFriendshipErr', err);
				SamSequelize.handleError(req, res, err);
			});
	},

	inviteBulk(req, res) {
		const invitations = req.body.map(e => ({ inviteeId: e, inviterId: req.tokenPayload.id }));
		models.invite
			.bulkCreate(invitations)
			.then(invites => {
				let count = 0;
				models.interaction
					.bulkCreate(
						invitations.map(e => ({
							interactionTypeId: 1,
							inviteId: invites[count++].dataValues.id,
							userId: req.tokenPayload.id,
							inviterId: req.tokenPayload.id,
							receiverId: e.inviteeId
						}))
					)
					.then(() => {
						res.json(invites);
					})
					.catch(err => {
						logger.log('InviteBulkFriendshipErr', err);
						SamSequelize.handleError(req, res, err);
					});
			})
			.catch(err => {
				logger.log('InviteBulkFriendshipErr', err);
				SamSequelize.handleError(req, res, err);
			});
	},

	emailInvite(req, res) {
		models.externalInvite
			.bulkCreate(duplicateObjInList({ inviterId: req.tokenPayload.id }, req.body.length))
			.then(invites => {
				for (let i = 0; i < invites.length; i++) {
					Email.send({
						to: req.body[i].email,
						subject: `${req.tokenPayload.fName} wants to share their Jurnie with you!`,
						template: 'invite',
						values: {
							inviteeName: req.body[i].name,
							inviterName: req.tokenPayload.fName,
							url: `www.jurnie.com/#!/signup?emailInviteId=${invites[i].dataValues.id}`
						}
					});
				}
				res.json({ message: 'success' });
			})
			.catch(err => {
				logger.log('EmailInviteFriendshipErr', err);
				res.status(500).json({ message: 'Failed to create invitation' });
			});
	},

	fbInvite(req, res) {
		models.externalInvite
			.create({ inviterId: req.tokenPayload.id })
			.then(invite => res.json(invite))
			.catch(err => {
				logger.log('FBInviteFriendshipErr', err);
				SamSequelize.handleError(req, res, err);
			});
	},

	getEmailInviter(req, res) {
		models.externalInvite
			.findById(req.params.emailInviteId, {
				include: [
					{ model: models.user, as: 'inviter', attributes: ['id', 'firstName', 'lastName', 'profilePic'] }
				]
			})
			.then(invite => {
				res.json(invite);
			})
			.catch(err => {
				logger.log('GetEmailInviterErr', err);
				SamSequelize.handleError(req, res, err);
			});
	},

	getFBInviter(req, res) {
		models.externalInvite
			.findById(req.params.fbInviteId, {
				include: [
					{ model: models.user, as: 'inviter', attributes: ['id', 'firstName', 'lastName', 'profilePic'] }
				]
			})
			.then(invite => {
				res.json(invite);
			})
			.catch(err => {
				logger.log('GetFBInviterErr', err);
				SamSequelize.handleError(req, res, err);
			});
	},

	accept(req, res) {
		models.invite
			.findById(req.params.inviteId)
			.then(invite => {
				models.invite
					.destroy({
						where: {
							id: invite.dataValues.id
						}
					})
					.then(function(deletedRecords) {
						models.friendship
							.create({
								frienderId: invite.dataValues.inviterId,
								friendeeId: invite.dataValues.inviteeId
							})
							.then(function(newFriendship) {
								models.friendship
									.create({
										frienderId: invite.dataValues.inviteeId,
										friendeeId: invite.dataValues.inviterId
									})
									.then(function(newFriendship) {
										models.interaction
											.create({
												interactionTypeId: 2,
												inviterId: invite.dataValues.inviterId,
												userId: req.tokenPayload.id
											})
											.then(blah => {
												models.interaction
													.create({
														interactionTypeId: 2,
														inviterId: req.tokenPayload.id,
														userId: invite.dataValues.inviterId
													})
													.then(blah => res.json(newFriendship))
													.catch(err => {
														logger.log('AcceptFriendshipErr', err);
														SamSequelize.handleError(req, res, err);
													});
											})
											.catch(err => {
												logger.log('AcceptFriendshipErr', err);
												SamSequelize.handleError(req, res, err);
											});
									})
									.catch(err => {
										logger.log('AcceptFriendshipErr', err);
										SamSequelize.handleError(req, res, err);
									});
							})
							.catch(err => {
								logger.log('AcceptFriendshipErr', err);
								SamSequelize.handleError(req, res, err);
							});
					})
					.catch(err => {
						logger.log('AcceptFriendshipErr', err);
						SamSequelize.handleError(req, res, err);
					});
			})
			.catch(err => {
				logger.log('AcceptFriendshipErr', err);
				SamSequelize.handleError(req, res, err);
			});
	},

	decline(req, res) {
		models.invite
			.destroy({
				where: {
					id: req.params.inviteId
				}
			})
			.then(function(deletedRecords) {
				res.status(200).json(deletedRecords);
			})
			.catch(err => {
				logger.log('DeclineFriendshipErr', err);
				SamSequelize.handleError(req, res, err);
			});
	},

	friendshipId(req, res, next, id) {
		req.friendshipId = id;
		next();
	}
};

function duplicateObjInList(obj, times) {
	const list = [];
	for (var i = 0; i < times; i++) {
		list.push(obj);
	}
	return list;
}
