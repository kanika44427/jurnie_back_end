'use strict';

module.exports = function(sequelize, DataTypes) {
	var CareerInquiry = sequelize.define(
		'careerInquiry',
		{
			id: {
				type: DataTypes.UUID,
				defaultValue: DataTypes.UUIDV1,
				primaryKey: true
			},
			name: {
				type: DataTypes.STRING,
				allowNull: false,
				validate: {
					notEmpty: true
				}
			},
			location: {
				type: DataTypes.STRING,
				allowNull: false,
				validate: {
					notEmpty: true
				}
			},
			email: {
				type: DataTypes.STRING,
				allowNull: false,
				validate: {
					notEmpty: true
				}
			},
			phone: {
				type: DataTypes.STRING,
				allowNull: true,
				validate: {
					notEmpty: false
				}
			},
			note: {
				type: DataTypes.STRING,
				allowNull: true,
				validate: {
					notEmpty: false
				}
			},
			attachment: {
				type: DataTypes.STRING,
				allowNull: true,
				validate: {
					notEmpty: true
				}
			}
		},
		{
			timestamps: true,
			paranoid: true,
			freezeTableName: true,
			classMethods: {
				associate: function(models) {}
			}
		}
	);
	return CareerInquiry;
};
