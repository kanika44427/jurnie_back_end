// const ua = require('universal-analytics'),
// 	trackingId = require('../config/options').google.tracking;

// module.exports = {
// 	event: event
// };

// function event(type, val, userId) {
// 	const visitor = ua(trackingId, userId);
// 	visitor
// 		.event(type, 1, function(err) {
// 			console.log(`tracking err(${type}):`, err);
// 		})
// 		.send();
// }

const ua = require('universal-analytics');
const bluebird = require('bluebird');

const GOOGLE_ANALYTICS_ID = require('../config/options').google.tracking;

class GoogleAnalytics {
	constructor() {}

	event(id, category, action, label) {
		const visitor = ua(GOOGLE_ANALYTICS_ID, id, { strictCidFormat: false });
		bluebird.promisifyAll(visitor);
		visitor.eventAsync(category, action, label).catch(error => console.error(error));
	}
}

module.exports = new GoogleAnalytics();
