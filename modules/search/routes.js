const router = require('express').Router({ mergeParams: true }),
	userAuth = require('../user/auth.js'),
	controller = require('./controller.js');

router.route('/').get(userAuth.isLoggedIn, controller.searchPlaces);
router.route('/nearby').get(userAuth.isLoggedIn, controller.searchNearby);
router.route('/geocode').get(userAuth.isLoggedIn, controller.reversePlaceId);

module.exports = router;
