var router = require('express').Router({ mergeParams: true }),
	userAuth = require('../user/auth.js'),
	controller = require('./controller.js');

router
	.route('/')
	.post(userAuth.isAdmin, controller.add)
	.get(userAuth.isLoggedIn, controller.listRandom);

router
	.route('/:cityId')
	.get(controller.grab)
	.put(userAuth.isAdmin, controller.edit)
	.delete(userAuth.isAdmin, controller.remove);

router.param('cityId', controller.cityId);

module.exports = router;
