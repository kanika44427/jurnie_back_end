'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
      var models = require ('../models');
        return models.userType.bulkCreate ([
            {id:1,description: 'admin'},
            {id:3,description: 'user'}
        ]);
  },

  down: function (queryInterface, Sequelize) {
    var models = require('../models');
    return queryInterface.bulkDelete('userType', null, {});
  }
};
