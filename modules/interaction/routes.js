var router = require('express').Router({ mergeParams: true }),
	auth = require('./auth.js'),
	userAuth = require('../user/auth.js'),
	controller = require('./controller.js');

router.route('/').get(userAuth.isLoggedIn, controller.list);

router.route('/:interactionId/mark-as-read').put(userAuth.isLoggedIn, controller.markAsRead);
router.route('/:interactionId').get(userAuth.isLoggedIn, controller.grab);

router.param('interactionId', controller.interactionId);

module.exports = router;
