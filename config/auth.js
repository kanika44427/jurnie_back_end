const ONE_WEEK_SECONDS = 604800;

const secret = require('./options.js').token.secret,
	samAuth = require('sam-auth'),
	SamSequelize = require('sam-sequelize'),
	models = require('../sequelize/models'),
	auth = samAuth({
		tokens: {
			user: {
				maxAge: ONE_WEEK_SECONDS,
				props: ['fName', 'lName', 'email', 'role', 'id'],
				httpOnly: process.env.NODE_ENV === 'production',
				secure: process.env.NODE_ENV === 'production',
				role: 'role',
				isCookie: false
			}
		},
		secret: secret,
		ownership: {
			user: function(req, res, id, next) {
				next({ success: req.tokenPayload.id === id });
			},
			group: function(req, res, id, next) {
				models.group
					.findById(id)
					.then(function(group) {
						group
							.getOrganizers()
							.then(function(organizers) {
								for (let i = 0; i < organizers.length; i++) {
									if (organizers[i].dataValues.id === req.tokenPayload.id)
										return next({ success: true });
								}
								next({ success: false });
							})
							.catch(function(err) {
								SamSequelize.handleError(req, res, err);
							});
					})
					.catch(function(err) {
						SamSequelize.handleError(req, res, err);
					});
			},
			event: function(req, res, id, next) {
				models.event
					.findById(id, {
						include: [
							{
								model: models.group,
								include: [
									{
										model: models.group_organizer,
										as: 'organizers'
									}
								]
							}
						]
					})
					.then(function(event) {
						event.dataValues.group.dataValues.organizers.dataValues.foreach(function(organizer) {
							if (organizer.dataValues.organizerId === req.tokenPayload.id)
								return next({ success: true });
						});
						next({ success: false });
					})
					.catch(function(err) {
						SamSequelize.handleError(req, res, err);
					});
			},
			invite: function(req, res, id, next) {
				models.invite
					.findById(id)
					.then(function(invite) {
						next({
							success: invite.inviteeId === req.tokenPayload.id
						});
					})
					.catch(function(err) {
						SamSequelize.handleError(req, res, err);
					});
			},
			pin: function(req, res, id, next) {
				models.pin
					.findById(id)
					.then(pin => next({ success: pin.dataValues.userId === req.tokenPayload.id }))
					.catch(err => SamSequelize.handleError(req, res, err));
			}
		}
	});

module.exports = auth;
