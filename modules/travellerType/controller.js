// Invoke "strict" JavaScript mode
'use strict';

// Load the module dependencies
const models = require('../../sequelize/models'),
	logger = require('../../services/log'),
	SamSequelize = require('sam-sequelize');

module.exports = {
	list(req, res) {
		models.travellerType
			.findAll()
			.then(function(travellerTypes) {
				res.status(200).json(travellerTypes);
			})
			.catch(err => {
				logger.log('ListTravellerTypeErr', err);
				SamSequelize.handleError(req, res, err);
			});
	},

	add(req, res) {
		models.travellerType
			.create(req.body)
			.then(function(travellerType) {
				res.status(200).json(travellerType);
			})
			.catch(err => {
				logger.log('AddTravellerTypeErr', err);
				SamSequelize.handleError(req, res, err);
			});
	},

	edit(req, res) {
		models.travellerType
			.update(req.body, {
				where: {
					id: req.params.travellerTypeId
				}
			})
			.then(function(updatedRecords) {
				res.status(200).json(updatedRecords);
			})
			.catch(err => {
				logger.log('EditTravellerTypeErr', err);
				SamSequelize.handleError(req, res, err);
			});
	},

	grab(req, res) {
		models.travellerType
			.findById(req.params.travellerTypeId, {
				include: [models.userType]
			})
			.then(function(travellerType) {
				res.status(200).json(travellerType);
			})
			.catch(err => {
				logger.log('EditTravellerTypeErr', err);
				SamSequelize.handleError(req, res, err);
			});
	},

	remove(req, res) {
		models.travellerType
			.destroy({
				where: {
					id: req.params.travellerTypeId
				}
			})
			.then(function(deletedRecords) {
				res.status(200).json(deletedRecords);
			})
			.catch(err => {
				logger.log('RemoveTravellerTypeErr', err);
				SamSequelize.handleError(req, res, err);
			});
	},

	travellerTypeId(req, res, next, id) {
		req.travellerTypeId = id;
		next();
	}
};
