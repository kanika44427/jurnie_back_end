'use strict';

module.exports = {
    up: function (queryInterface, Sequelize) {
        var models = require ('../models');
        return models.interactionType.bulkCreate([
            {
                id: 1,
                description: 'invite',
            },
            {
                id: 2,
                description: 'accept',
            },
            {
                id: 3,
                description: 'pin',
            },
            {
                id: 4,
                description: 'note',
            }
        ]);
    },

    down: function (queryInterface, Sequelize) {
        var models = require('../models');
        return queryInterface.bulkDelete('interactionType', null, {});
    }
};
