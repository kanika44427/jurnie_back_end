var router = require('express').Router({ mergeParams: true }),
	userAuth = require('../user/auth.js'),
	controller = require('./controller.js');

router.route('/').post(controller.add).get(userAuth.isAdmin, controller.list);

router.route('/resume').post(controller.resume);

router.route('/:careerInquiryId').get(userAuth.isAdmin, controller.grab).delete(userAuth.isAdmin, controller.remove);

router.param('careerInquiryId', controller.careerInquiryId);

module.exports = router;
