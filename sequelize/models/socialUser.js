'use strict';

module.exports = function(sequelize, DataTypes) {
	var socialUser = sequelize.define(
		'socialUser',
		{			
			firstName: {
				type: DataTypes.STRING,
				validate: {
					notEmpty: true
				},
				allowNull: false
			},			
			lastName: {
				type: DataTypes.STRING,
				validate: {
					notEmpty: true
				},
				allowNull: false
			},			
			email: {
				type: DataTypes.STRING,
				unique: true,
				validate: {
					notEmpty: true,
					//isEmail: false
				},
				allowNull: false
			},
			userType: {
				type: DataTypes.STRING,
				validate: {
					notEmpty: true
				},
				allowNull: false
			},
			providerId: {
				type: DataTypes.STRING,
				validate: {
					notEmpty: true
				},
				allowNull: false
			},
			profileImage: {
				type: DataTypes.STRING,
				allowNull: false,
				validate: {
					notEmpty: true
				},
				defaultValue: 'assets/Annonymous.png'
			}
		},
		{
			timestamps: true,
			paranoid: true,
			freezeTableName: true,
			classMethods: {
				associate: function(models) {
				}
			}
		}
		
	);
	sequelize.sync();
	return socialUser;
};
