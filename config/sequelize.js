module.exports = {
	dev: {
		url: process.env.DATABASE_URL || getDBUrl(),
		dialect: 'mysql',
		pool: process.env.MIGRATION ? null : { max: 10 },
		logging: true
	},
	development: {
		url: process.env.DATABASE_URL || getDBUrl(),
		dialect: 'mysql',
		pool: process.env.MIGRATION ? null : { max: 10 },
		logging: true
	},
	local: {
		url: process.env.DATABASE_URL || getDBUrl(),
		dialect: 'mysql',
		pool: process.env.MIGRATION ? null : { max: 10 },
		logging: true
	},
	production: {
		url: process.env.DATABASE_URL || getDBUrl(),
		dialect: 'mysql',
		pool: process.env.MIGRATION ? null : { max: 10 },
		logging: false
	},
	staging: {
		url: process.env.DATABASE_URL || getDBUrl(),
		dialect: 'mysql',
		pool: process.env.MIGRATION ? null : { max: 10 },
		logging: false
	},
	test: {
		url: process.env.DATABASE_URL || getDBUrl(),
		dialect: 'mysql',
		pool: process.env.MIGRATION ? null : { max: 10 },
		logging: false
	}
};

function getDBUrl() {
	const test = {
		username: 'be5652fa519c35',
		password: 'b815a591',
		host: 'us-cdbr-iron-east-03.cleardb.net',
		path: '/heroku_0e47d024972c996'
	};

	const prod = {
		username: process.env.RDS_USERNAME,
		password: process.env.RDS_PASSWORD,
		host: process.env.RDS_HOSTNAME,
		port: process.env.RDS_PORT,
		path: '/innodb'
	};
	return `mysql://${prod.username || test.username}:${prod.password || test.password}@${prod.host ||
		test.host}${prod.username ? prod.path : test.path}`;
}
